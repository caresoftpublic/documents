
## **Thông tin API getCalls**
** GET:  /{domain}/api/v1/calls?
<table>
  <tr>
   <td><strong>STT</strong>
   </td>
   <td><strong>Tên trường</strong>
   </td>
   <td><strong>Kiểu dữ liệu</strong>
   </td>
   <td><strong>Chú thích</strong>
   </td>
  </tr>
  <tr>
   <td>1
   </td>
   <td>code
   </td>
   <td>char
   </td>
   <td>Trạng thái api OK: Thành công, NOK: Thất bại
   </td>
  </tr>
  <tr>
   <td>2
   </td>
   <td>numFound
   </td>
   <td>int
   </td>
   <td>Số bản ghi được tìm thấy
   </td>
  </tr>
  <tr>
   <td>3
   </td>
   <td>calls
   </td>
   <td>array
   </td>
   <td>Mảng dữ liệu kết quả tương tứng
   </td>
  </tr>
  <tr>
   <td>3.1
   </td>
   <td>customer_id
   </td>
   <td>int
   </td>
   <td>ID khách hàng được gọi/gọi trên hệ thống caresoft  (duy nhất trên hệ thống) - Xem chi tiết khách hàng bằng api /api/v1/contacts/{customer_id} 
   </td>
  </tr>
  <tr>
   <td>3.2
   </td>
   <td>call_id
   </td>
   <td>nvarchar
   </td>
   <td>ID cuộc gọi (duy nhất cho từng cuộc gọi)
   </td>
  </tr>
  <tr>
   <td>3.3
   </td>
   <td>caller
   </td>
   <td>nvarchar
   </td>
   <td>Số gọi/ nhánh gọi
   </td>
  </tr>
  <tr>
   <td>3.4
   </td>
   <td>called
   </td>
   <td>nvarchar
   </td>
   <td>Số nhận cuộc gọi/ Nhánh nhận 
   </td>
  </tr>
  <tr>
   <td>3.5
   </td>
   <td>user_id
   </td>
   <td>int
   </td>
   <td>ID định danh của agent tiếp nhận hoặc thực hiện cuộc gọi (duy nhất trên hệ thống)
- Xem chi tiết  bằng api /api/v1/contacts/{user_id} 
   </td>
  </tr>
  <tr>
   <td>3.6
   </td>
   <td>agent_id
   </td>
   <td>Int
   </td>
   <td>Số IP phone của agent tiếp nhận hoặc thực hiện cuộc gọi
   </td>
  </tr>
  <tr>
   <td>3.7
   </td>
   <td>group_id
   </td>
   <td>int
   </td>
   <td>ID phòng ban của agent tiếp nhận / thực hiện cuộc gọi
   </td>
  </tr>
  <tr>
   <td>3.8
   </td>
   <td>call_type
   </td>
   <td>int
   </td>
   <td>Kiểu cuộc gọi
   0: Gọi ra 
1: Gọi vào
   </td>
  </tr>
  <tr>
   <td>3.9
   </td>
   <td>start_time
   </td>
   <td>dateTime (YYYY-MM-DD HH:mm:ss)
   </td>
   <td>Thời điểm bắt đầu 
   </td>
  </tr>
  <tr>
   <td>3.10
   </td>
   <td>call_status
   </td>
   <td>nvarchar
   </td>
   <td>Trạng thái cuộc gọi
“miss”:  Nhỡ,
“meetAgent”: Gặp được agent
   </td>
  </tr>
  <tr>
   <td>3.11
   </td>
   <td>end_time
   </td>
   <td>dateTime
(YYYY-MM-DD HH:mm:ss)
   </td>
   <td>Thời điểm kết thúc cuộc gọi 
   </td>
  </tr>
  <tr>
   <td>3.12
   </td>
   <td>wait_time
   </td>
   <td>time (HH:mm:ss)
   </td>
   <td>Thời gian chờ
   </td>
  </tr>
  <tr>
   <td>3.13
   </td>
   <td>hold_time
   </td>
   <td>time (HH:mm:ss)
   </td>
   <td>Thời gian giữ máy
   </td>
  </tr>
  <tr>
   <td>3.14
   </td>
   <td>talk_time
   </td>
   <td>time
(HH:mm:ss)
   </td>
   <td>Thời gian đàm thoại
   </td>
  </tr>
  <tr>
   <td>3.15
   </td>
   <td>end_status
   </td>
   <td>nvarchar
   </td>
   <td>Trạng thái kết thúc cuộc gọi “cus”: Từ khách hàng, “system”: Từ hệ thống caresoft
   </td>
  </tr>
  <tr>
   <td>3.16
   </td>
   <td>ticket_id
   </td>
   <td>int
   </td>
   <td>Ticket ID tương ứng với cuộc gọi. 
   Lấy thông tin chi tiết qua api  {domain}/api/v1/tickets/{ticket_id}
   </td>
  </tr>
  <tr>
   <td>3.17
   </td>
   <td>last_agent_id
   </td>
   <td>int
   </td>
   <td>Số IP phone của agent cuối cùng tiếp nhận cuộc gọi (trong trường hợp cuộc gọi sẽ trượt qua các nhánh mà các agent khác không nhận thì người cuối cùng nhận được cuộc gọi sẽ được ghi nhận lại)
   </td>
  </tr>
  <tr>
   <td>3.18
   </td>
   <td>last_user_id
   </td>
   <td>int
   </td>
   <td>ID định danh của agent cuối cùng tiếp nhận cuộc gọi
   </td>
  </tr>
  <tr>
   <td>3.19
   </td>
   <td>call_survey
   </td>
   <td>nvarchar
   </td>
   <td>Trạng thái cuộc gọi khảo sát
   “NO” không phải
   “YES” Có
   </td>
  </tr>
  <tr>
   <td>3.20
   </td>
   <td>call_survey_result
   </td>
   <td>nvarchar
   </td>
   <td>Nội dung kịch bản thu được từ cuộc gọi khảo sát
   </td>
  </tr>
  <tr>
   <td>3.21
   </td>
   <td>miss_reason
   </td>
   <td>nvarchar
   </td>
   <td>Trạng thái gọi nhỡ
“missed_customer” : Nhỡ do khách không bắt máy 
“missed_agent_device”: Nhỡ do thiết bị của agent 
“missed_agent_reject” : Nhỡ do agent từ chối
“missed_agent_timeout”: Nhỡ do agent không bắt máy
   </td>
  </tr>
  <tr>
   <td>3.22
   </td>
   <td>path_download
   </td>
   <td>nvarchar
   </td>
   <td>Url file ghi âm dùng để tải về (nếu cuộc gọi được kết nối thành công, Lưu ý file ghi âm sẽ xuất hiện sau cuộc gọi 5 phút)
   </td>
  </tr>
  <tr>
   <td>3.33
   </td>
   <td>path
   </td>
   <td>nvarchar
   </td>
   <td>Url file ghi âm (streaming)  (nếu cuộc gọi được kết nối thành công, Lưu ý file ghi âm sẽ xuất hiện sau cuộc gọi 5 phút)
   </td>
  </tr>
</table>
Sample response:

    {
        "code": "OK",
        "numFound": 33,
        "calls": [{

            "id": 204508258,
            "customer_id": 111669583,
            "call_id": "20211010010236-OUTHUCGB-101441",
            "path": "https:\/\/f6.caresoft.vn:19991\/f\/bbbddđff\/\/6161d969832e600cc60ddcb7\/6b0e12c9b3304d8e7878e9f4e1745194\/db15ea7d12a2d225791902f94401addf.mp3",
            "path_download": "https:\/\/f6.caresoft.vn:19991\/f\/b6a0d15a9cf87ccssdsdsdc6cef402e5131f24f6b\/\/6161d969832e600cc60ddcb7\/cd0490cefca753018d9f55bf9fff4739\/db15ea7d12a2d225791902f94401addf.mp3",
            "caller": "0827223692",
            "called": "842871099666",
            "user_id": "79179307",
            "agent_id": "21857",
            "group_id": 9716, 
            "call_type": 1,
            "start_time": "2021-10-10 01:02:36",
            "call_status": "meetAgent",
            "end_time": "2021-10-10 01:02:55",
            "wait_time": "00:00:10",
            "hold_time": "00:00:00",
            "talk_time": "00:00:09",
            "end_status": "system",
            "ticket_id": 260078689,
            "last_agent_id": "21857",
            "last_user_id": 79179307,
            "call_survey": "NO",
            "call_survey_result": null,
            "missed_reason": null
        }]
    }


## **Thông tin API getChats**

** GET:  /{domain}/api/v1/chats?

<table>
  <tr>
   <td><strong>STT</strong>
   </td>
   <td><strong>Tên trường</strong>
   </td>
   <td><strong>Kiểu dữ liệu</strong>
   </td>
   <td><strong>Chú thích</strong>
   </td>
  </tr>
  <tr>
   <td>1
   </td>
   <td>code
   </td>
   <td>char
   </td>
   <td>Trạng thái api OK: Thành công, NOK: Thất bại
   </td>
  </tr>
  <tr>
   <td>2
   </td>
   <td>numFound
   </td>
   <td>int
   </td>
   <td>Số bản ghi được tìm thấy
   </td>
  </tr>
  <tr>
   <td>3
   </td>
   <td>chats
   </td>
   <td>array
   </td>
   <td>Mảng dữ liệu kết quả tương tứng
   </td>
  </tr>
  <tr>
   <td>3.1
   </td>
   <td>ticket_id
   </td>
   <td>
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>3.2
   </td>
   <td>ticket_no
   </td>
   <td>
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>3.3
   </td>
   <td>customer_id
   </td>
   <td>int
   </td>
   <td>ID khách hàng được gọi/gọi trên hệ thống caresoft  (duy nhất trên hệ thống) - Xem chi tiết khách hàng bằng api /api/v1/contacts/{customer_id} 
   </td>
  </tr>
  <tr>
   <td>3.4
   </td>
   <td>group_name
   </td>
   <td>nvarchar
   </td>
   <td>Phòng ban của agent nhận chat
   </td>
  </tr>
  <tr>
   <td>3.5
   </td>
   <td>conversation_id
   </td>
   <td>nvarchar
   </td>
   <td>ID của luồng chat
   </td>
  </tr>
  <tr>
   <td>3.6
   </td>
   <td>cus_email
   </td>
   <td>email
   </td>
   <td>Email của khách
   </td>
  </tr>
  <tr>
   <td>3.7
   </td>
   <td>cus_name
   </td>
   <td>nvarchar
   </td>
   <td>Tên khách hàng
   </td>
  </tr>
  <tr>
   <td>3.8
   </td>
   <td>cus_phone
   </td>
   <td>mvarchar
   </td>
   <td>Số điện thoại khách 
   </td>
  </tr>
  <tr>
   <td>3.9
   </td>
   <td>start_time
   </td>
   <td>dateTime
   </td>
   <td>Thời điểm bắt đầu
   </td>
  </tr>
  <tr>
   <td>3.10
   </td>
   <td>end_time
   </td>
   <td>dateTime
   </td>
   <td>Thời điểm kết thúc
   </td>
  </tr>
  <tr>
   <td>3.11
   </td>
   <td>chat_duration
   </td>
   <td>int
   </td>
   <td>Thời lượng chat
   </td>
  </tr>
  <tr>
   <td>3.12
   </td>
   <td>agent_email
   </td>
   <td>email
   </td>
   <td>Email agent
   </td>
  </tr>
  <tr>
   <td>3.13
   </td>
   <td>agent_name
   </td>
   <td>nvarchar
   </td>
   <td>Tên agent
   </td>
  </tr>
  <tr>
   <td>3.14
   </td>
   <td>ring_time
   </td>
   <td>time
   </td>
   <td>Thời gian ring
   </td>
  </tr>
  <tr>
   <td>3.15
   </td>
   <td>meet_time
   </td>
   <td>time
   </td>
   <td>Thời gian gặp
   </td>
  </tr>
  <tr>
   <td>3.16
   </td>
   <td>waitTime
   </td>
   <td>time
   </td>
   <td>Thời gian chờ
   </td>
  </tr>
  <tr>
   <td>3.17
   </td>
   <td>answer_time
   </td>
   <td>time
   </td>
   <td>Thời gian trả lời
   </td>
  </tr>
  <tr>
   <td>3.18
   </td>
   <td>chat_status
   </td>
   <td>nvarchar
   </td>
   <td>LBL_CHAT_STATUS_MEET : nhận
   LBL_CHAT_STATUS_MISS : nhỡ
   </td>
  </tr>
  <tr>
   <td>3.19
   </td>
   <td>landing_page
   </td>
   <td>nvarchar
   </td>
   <td>Landingpag đường dẫn mà user vào đầu tiên
   </td>
  </tr>
  <tr>
   <td>3.20
   </td>
   <td>referrer
   </td>
   <td>nvarchar
   </td>
   <td>đường dẫn mà user từ trang nào chuyển hướng sang
   </td>
  </tr>
  <tr>
   <td>3.21
   </td>
   <td>is_trigger
   </td>
   <td>nvarchar
   </td>
   <td>chat này có phải reply từ trigger hay khách chủ động chat lên
LBL_CHAT_CUSTOMER_REQUEST_NORMAL : khách chủ động 
LBL_CHAT_CUSTOMER_REQUEST_TRIGGER : reply từ trigger
   </td>
  </tr>
</table>


Sample response:

    {
    "code": "OK",
    "numFound": 33,
    "chats": [{
            "ticket_id": 260078254,
            "ticket_no": 65732,
            "customer_id": 122566884,
            "group_name": "K\u1ebf to\u00e1n",
            "conversation_id": "20211010002705-BCPAALQJ-15019",
            "cus_email": "",
            "cus_name": "ssadsad",
            "cus_phone": "033699552212",
            "start_time": "2021-10-10 00:27:05",
            "end_time": "2021-10-10 00:27:43",
             "chat_duration": 37,
            "agent_email": "hungnx1@caresoft.vn",
            "agent_name": "hungnx1",
            "ring_time": "2021-10-10 00:27:06",
            "meet_time": "2021-10-10 00:27:08",
            "waitTime": "00:00:03",
            "answer_time": "00:00:34",
            "chat_status": "LBL_CHAT_STATUS_MEET",
            "landing_page": "https:\/\/web2.caresoft.vn\/",
            "referrer": null,
            "is_trigger": "LBL_CHAT_CUSTOMER_REQUEST_NORMAL"
        }]
    }
