**Bước 1: Khởi tạo tài khoản**
+ Tạo tài khoản (domain), khai báo agent trên Caresoft
+ Lấy access_key của domain và lưu lại
+ Lấy danh sách đầu số gọi ra và lưu lại (Optional)

**Bước 2: Lập trình tích hợp**
Khi cần click to call, thực hiện mở 1 popup với url có dạng sau

> https://c2c.caresoft.vn/{domain}/c2call?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9..........&number=09876543210

Trong đó:
- `{domain}`: tên domain tạo ở bước 1
- `number`: là số điện thoại cần gọi
- `token`: là chuỗi mã hóa theo chuẩn **JWT** (xem thêm ở https://jwt.io/), bạn sẽ cần lập trình để gen ra đoạn mã hóa này với thuật toán `HS256`, secret key là `access_key` lấy được ở bước 1 và phần Payload có dạng như bên dưới:

**Payload:**

```
{
	"ipphone": "5000", // ext của agent
	"expired": "2017-07-30 00:00:00" // thời hạn của token, Caresoft sẽ validate tham số này, nếu không truyền sang sẽ có mặc định,
	“callout_id”: ”{callout_id}”   // Id của đầu số muốn gọi ra, nếu không truyền vào sẽ lấy đầu số default được gán cho agent để gọi ra
}
```




**Ví dụ gen JWT token:**

 Ở đây sử dụng ngôn ngữ **PHP** và dùng thư viện: https://github.com/firebase/php-jwt để gen JWT token

```
$key = “access_key”;
$payload = array(
                "ipphone" => "5000",
                "expired" => "2017-07-30 00:00:00"
	    );
$token = JWT::encode($payload, $key);
```


**Ví dụ:** Khi click vào 1 số điện thoại, 1 popup sẽ được mở ra theo hướng dẫn như trên với token và số điện thoại là tham số được truyền vào

**Bước 1:**

![c2c buoc 1](../../pictures/click_to_call_buoc_1.png)

**Bước 2:** Popup sẽ được mở ra và gọi đến số điện thoại cần gọi

![c2c buoc 2](../../pictures/click_to_call_buoc_2.png)
