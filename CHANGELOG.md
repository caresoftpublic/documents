# Năm 2023

## Tháng 06-2023

### 23-06-2023

 #### 1. Added
- Nâng cấp chèn được tham số trong tiêu đề email gửi chiến dịch
- Nâng cấp API lấy dữ liệu về để làm báo cáo QA
- Nâng cấp API update thời hạn xử lý phiếu ghi
- Nâng cấp cấu hình filter để setup theo vai trò
- Bổ sung kịch bản gửi tin giao dịch và tin truyền thông
    - Tin nhắn văn bản (có thể thêm nút bấm)
    - Tin nhắn truyền thông
    - Tin nhắn giao dịch
- Bổ sung action nhân bản ticket trong giao diện bộ kích hoạt
- Nâng cấp đổi ngôn ngữ mục Kho ảnh chung theo cấu hình của tài khoản
- Bổ sung giao diện session manager (quản lý phiên đăng nhập)
- Thêm kiểu tin nhắn template zalo cho tin giao dịch và tin truyền thông
- Bổ sung cập nhật ticket ccs trên solr khi merge user
- Nâng cấp import hàng loạt Tổ Chức lên hệ thống
- Nâng cấp không trả về event tạo user khi user đã tồn tại trên hệ thống
- Nâng cấp phân phối tương tác zalo: phân phối đúng cho chuyên viên follow khi gửi tin ZNS ra và khách hàng phản hồi lại

 #### 2. Fixed
 - Fix lỗi hiển thị ngôn ngữ
 - Fix lỗi bảo mật

### 09-06-2023

 #### 1. Added
- Nâng cấp cấu hình IVR cho phép cấu hình file sai phím
- Nâng cấp kịch bản chỉ chọn được 1 đầu số và cho nhân bản kịch bản IVR trên giao diện
- Mở rộng thêm số lượng ký tự cho kiểu dữ liệu "Văn bản" trong thuộc tính động Phiếu ghi: Trường từ 21-50 tăng số lượng lên 500 ký tự
- Bổ sung định dạng số điện thoại nước ngoài
- Bổ sung hướng dẫn tích hợp Voice Api
- Nâng cấp thay đổi cấu hình agent không view được chat chưa xử lý: đổi sang phân quyên theo vai trò, chuyên viên cụ thể
- Nâng cấp chỉnh lại phần hiển thị thông tin khi tên chuyên viên bị dài trong lịch sử bình luận
- Nâng cấp hiển thị kênh trong giao diện giám sát

 #### 2. Fixed
 - Fix tìm kiêm theo chủ đề chứa không ra được dữ liệu
 - Fix lỗi ghi nhận sai SLA v2

## Tháng 05-2023

### 26-05-2023

 #### 1. Added
- Nâng cấp bổ sung lựa chọn thang điểm âm trong kich bản QA
- Nâng cấp thêm phần đồng bộ thông tin trường thuộc tính qua ERP khách hàng
- Update điều chỉnh được thứ tự cho bài viết Help center
- Nâng cấp livechat bổ sung cơ chế không bắt buộc điền số 0 ở số điện thoại
- Thay đổi cơ chế tự động ghi nhận số điện thoại trong báo cáo: ghi nhận được cả số điện thoại nước ngoài
- Bổ sung ẩn báo cáo tổng quan SLA v1 khi bật quyền dùng SLA v2
- Đánh giá chỉ số báo cáo thời gian chờ với trường hợp tích hợp với chatbot
- Update thêm tên khách hàng với trường hợp truyền thông tin không có tên qua livechat
- Bổ sung quản lý tài khoản quảng cáo Google

 #### 2. Fixed
- 

### 12-05-2023

 #### 1. Added
- Nâng cấp tăng số lượng trường động phiếu ghi lên 50 trường
- Nâng cấp thêm Api created_from
- Nâng cấp bổ sung file ghi âm và ticket id cho màn hình báo cáo nhân viên trong báo cáo smartdialer
- Update lại tài liệu API
- Thay đổi hiển thị báo cáo đánh giá khách hàng
    - Thêm chỉ số tổng khách hàng đánh giá ở biểu đồ
    - Thêm chỉ số tổng khách hàng ở bảng chi tiết
- Nâng cấp giao diện Livechat cho thiết bị Mobile, tin nhắn sẽ bắt đầu từ dưới lên thay vì bắt đầu từ trên xuống
- Nâng cấp phần bắt buộc để lại thông tin trong livechat
- Nâng cấp báo cáo chat: Ghi nhận chat nhỡ với trường hợp khách hàng là người chủ động thoát chat trước
- Nâng cấp tăng thời gian tạo chat nhỡ cho các tin đồng bộ - Tăng lên 10 ngày (hiện tại đang 3 ngày)

 #### 2. Fixed
- Fix bấm vào livechat bị treo: visitor bị ban ip, website dùng custom button bấm gọi vào mở box chat khi user đã bị ban cho nên gây lỗi
- Fix Lỗi xuất báo cáo lịch sử chat - Thời gian phản hồi lần đầu ra năm 1970

## Tháng 04-2023

### 27-04-2023

 #### 1. Added
- Nâng cấp đẩy thông tin thành phố và quận huyện qua API
- Nâng cấp ẩn số điện thoại trên chat
- Nâng cấp chặn không cho chuyên viên chủ động tạo thẻ tag
- Nâng cấp hiển thị lịch chat khách hàng bị merge
- Làm lại báo cáo thời gian chuyển đổi

 #### 2. Fixed
- Fix lọc được theo nhóm nguồn ở báo cáo sale funnel

### 21-04-2023

 #### 1. Added
- Bổ sung tự tạm dừng các trigger, ticket filter nếu trường động bị thay đổi giá trị hoặc thuộc tính từ cấu hình
- Chuẩn hóa lại các tham số thuôc tính quảng cáo trong cấu hình nguồn
- Bổ sung nguồn chi tiết cho các phiếu ghi khi chạy quảng cáo từ google
- Nâng cấp get được thông tin thời gian lấy số điện thoại của khách hàng qua API
- Bổ sung cấu hình phân phối agent theo trọng số. [Link](https://kb.caresoft.vn/support/hc#/?type=article&id=5271)
- Bổ sung báo cáo Sale Funnel báo cáo Lost/Unqualified Reasons
- Bổ sung phân quyền cho agent lọc được lịch sử chat như admin
- Bổ sung báo cáo Deal Won
- Cập nhật Facebook API Marketing

 #### 2. Fixed
- Fix lỗi đổi ngôn ngữ phải logout
- Fix lỗi không hiển thị được danh sách phiếu ghi
- Fix lỗi hiển thị sai thông tin bộ phận trong bộ lọc
- Lược bỏ những mục không còn hoạt động trong báo cáo Zalo-ZNS

### 06-04-2023

 #### 1. Added
- Tối ưu câu lệnh filter phiếu ghi
- Bổ sung giao diện tạo tài khoản theo Catology
-  Bổ sung cấu hình ẩn trạng thái Solved ở màn hình chi tiết phiếu ghi khi phiếu đang chuyển sang các giai đoạn open/pending
- Bổ sung ngôn ngữ giao diện tiếng nga
- Nâng cấp cảnh báo trong thời điểm cập nhật ticket mà người dùng bị merge
- Bổ sung phân trang cho nhóm nguồn
- Bổ sung báo cáo Sale Funnel làm phần báo cáo tỷ lệ chuyển đổi theo agent/group và báo cáo Lead/Deal theo Source
- Nâng cấp đổi id hội thoại web khi merge vào người dùng cũ

 #### 2. Fixed
- Fix lỗi không thay đổi thông tin thuộc tính ở trong bộ lọc khi đã sửa trường thuộc tính

## Tháng 03-2023

### 24-03-2023

 #### 1. Added
- Tách báo cáo Instagram riêng với Facebook
- Hiển thị giao diện liên kết phụ thuộc giữa các trường
- Bổ sung điều kiện phủ định cho trigger tự động gán nhóm nguồn
- Chặn trạng thái đảo ngược trạng thái Deals thành Leads
- Bổ sung cảnh báo khi thêm max 20 câu hỏi kịch bản QA
- Bổ sung API trả thông tin ID phiếu ghi cha bị thiếu
- Nâng cấp cơ chế để tìm được tất cả các email gửi ra từ một địa chỉ email
- Bổ sung xem chi tiết phiếu ghi từ màn hình báo cáo QA / bộ lọc phiếu QA bị khiếu nại
- Bổ sung thời gian time target ở màn hình chi tiết phiếu ghi
- Nâng cấp thêm lựa chọn ở trường thuộc tính trên App mobile
- Bổ sung hiển thị thêm thông báo hệ thống cho App mobile
- Bỏ update số điện thoại vào người dùng khi số đã tồn tại trên hệ thống trong trường hợp khách hàng Comment

 #### 2. Fixed
- Không hiển thị trạng thái kết thúc khi chỉnh sửa trường thuộc tính tiến trình cũ
- Nguồn chi tiết có Campaign_Name nhưng thiếu Campaign_ID

### 10-03-2023

 #### 1. Added
- Nâng cấp Ticket Form có thể chọn thông tin trường động của Khách hàng
- Nâng cấp màn hình cấu hình thuộc tính động phiếu ghi loại tiến trình:
    1. Đưa trạng thái kết thúc và thời gian tiến trình ra ngoài
    2. Nối trạng thái tiếp theo trong trường hợp có hai stage nối nhau
- Nâng cấp điều kiện CCs: có thể CC được với bất kỳ Email nào
- Nâng cấp filter comment phiếu ghi
-  Bổ sung báo cáo kết quả gửi ZNS theo chiến dịch
- Bổ sung ẩn số email/điện thoại của khách hàng trên giao diện pop up của click2call
- Bổ sung ngôn ngữ tiếng Cambodia trên Livechat
- Nâng cấp chọn thời gian báo cáo: Báo cáo lấy hết các tương tác thời gian 23:59:00 đến 23:59:59
- Nâng cấp không thay đổi trạng thái phiếu ghi khi gọi ra trên phiếu đang ở trạng thái khác New

 #### 2. Fixed
- Fix bug không hiển thị chữ ký khi gửi email
- Fix bug hiển thị danh sách chiến dịch trong báo cáo khi có nhiều chiến dịch
- Fix bug không hiển thị trường con khi lựa chọn qua lại giữa các giá trị thuộc tính cha
- Fix bug chọn thời gian theo tuần trong báo cáo bị bắt đầu từ chủ nhật

## Tháng 02-2023

### 24-02-2023

 #### 1. Added
- Bổ sung tính năng clone thiết lập filter
- Bổ sung menu Workflow nhúng iframe
- Nâng cấp tính năng QA Lead: QA Lead có quyền rà soát và chấm những ticket đã được QA chấm nhưng Agent chưa khiếu nại, hoặc ko có khiếu nại
- Bổ sung màn hình giao diện thông báo trên toolbar
- Bổ sung filter trường động cho báo cáo lịch sử cuộc gọi
- Nâng cấp giao diện báo cáo funel:
    1. Yêu cầu bổ sung thêm menu riêng của màn hình báo cáo
    + Tạo một menu riêng đưa các báo cáo Tỷ lệ chuyển đổi trường tiến trình, Báo cáo nhóm nguồn, Báo cáo nguồn chi tiết, vào menu đó
    + Vẫn giữ những báo cáo trên ở vị trí cũ

    2. Chỉnh sửa báo báo Tỷ lệ chuyển đổi trường tiến trình(Stage converted rate)
    + Trường hợp có 1cha mà nhiều con kiểu stage thì chi cần một combox( Báo cáo theo). Thể hiện theo kiểu tên cha/tên con
    + Trường hợp có nhiều cha thì hiển thị hai combox, một combox thể hiện tên cha
    + Có thể hiện giao diện khi chọn trường dữ liệu( nút tích/ bôi đậm)
    3. Bổ sung tỷ lệ lost rẽ nhánh ở các stage

 #### 2. Fixed
- Fix lỗi không đồng bộ bình luận

### 10-02-2023

 #### 1. Added
- Nâng cấp báo cáo SLA phiếu ghi và Báo cáo thời gian chờ chat: thời điểm tạo hội thoại được tính từ tin nhắn đầu tiên của khách chat vào
- Bổ sung mẫu trả lời nhanh Instagram
- Nâng cấp hiển thị tên dịch vụ trong filter bộ lọc phiếu ghi
- Nâng cấp bổ sung thêm điều kiện để Filter phiếu ghi và bộ lọc phiếu ghi có những điều kiện giống nhau
- Nâng cấp thay đổi hiển thị biểu đồ báo cáo SLA v2

 #### 2. Fixed
- Fix các trường như: trạng thái, mô tả, chủ đề... khi để trạng thái null thì phải truyền vào trạng thái mặc định
- Fix lỗi chặn SĐT nhưng vẫn gọi vào được
- Fix lỗi tính năng ẩn thông báo hệ thống lúc tiếp nhận chat
- Fixbug không chạy trigger/auto với điều kiện bằng tên chiến dịch
- Fix lỗi lệch trường dữ liệu khi xuất từ báo cáo lịch sử cuộc gọi
- Fix lỗi không phân phối theo last Agent trong cấu hình chuyên viên nhận trong bài viết

## Tháng 01-2023

### 18-01-2023

 #### 1. Added
- Nâng cấp thêm cấu hình giữ trạng thái solved phiếu ghi comment

### 13-01-2023

 #### 1. Added
- Bổ sung tự động phân phối theo thuộc tính quảng cáo
- Tích hợp ứng dụng Myspa
- Bổ sung search nguồn chi tiết theo ID
- Bổ sung quản lý bài post instagram
- Tích hợp ERP Oddo
- Update luồng xử lý comment facebook
    1. Update cho phép chọn trạng thái giống phần inbox, để dùng được macro.
    2. Bọ lọc nâng cấp cho thêm option: Đã private inbox cho khách -> mục đích để lọc bỏ các comment đã inbox mà không solved tickets.
- Bổ sung màn hình xử lý comment Instagram
- Yêu cầu chỉnh sửa giao diện màn hình chat và hiển thị thời gian Stage
    1. Bỏ option Đã xong thành New trong màn hình chat
    2. Chỉnh sửa giao diên hiển thị thời gian Stage: Hiện tại khi giá trị tên của trường dài thì thời gian không bằng nhau
- Điều chỉnh tính trung bình khi xuất từ biểu đồ phiếu ghi đánh giá
- Bổ sung cột Call ID vào báo cáo lịch sử cuộc gọi

#### 2. Fixed
- Fix giao diện màn hình chi tiết phiếu ghi bị lỗi giao diện khi các trường tên dài
- Fix lỗi phần setup IVR giao diện

# Năm 2022

## Tháng 12-2022

### 29-12-2022

 #### 1. Added
- Bổ sung tự động gán nguồn chi tiết vào nhóm nguồn
- Bổ sung hiển thị link sản phẩm khi khách hàng inbox từ Facebook Shop
- Bổ sung hiển thị được lịch sử chat của khách hàng khi bị merge người dùng
- Nâng cấp hiển thị rõ nguồn kênh chat
- Bổ sung tùy chỉnh quyền chặn số điện thoại theo vai trò, chuyên viên, bộ phận
- Bổ sung sort bảng trong các báo cáo
- Bổ sung tính năng đã xem tin nhắn


#### 2. Fixed
- Fix lỗi phần phân quyền cấu hình email: bật cấu hình email phải bật cấu hình SMS
- Fix bỏ chỉ số Cuộc gọi đang chờ dưới 45s và Tỉ lệ tiếp nhận cuộc gọi ( dưới 45s)
- Fix filter điều kiện tại báo cáo chi tiết của báo cáo Funel không hiển thị kết quả

### 16-12-2022

 #### 1. Added
- Nâng cấp gán được các quyền giám sát cho vai trò QA, QA Lead
- Nâng cấp bổ sung thêm xuất trường ID bài viết trong bộ lọc phiếu ghi
- Nâng cấp phần đồng bộ comment vào 1 ảnh trong 1 bài viết nhiều ảnh
- Nâng cấp ghi nhận trạng thái phản hồi từ ticket chat vào lịch sử chat
- Bổ sung kênh mới: Instagram. [Link](https://kb.caresoft.vn/support/hc#/?type=article&id=5180)
- Nâng cấp mẫu trả lời nhanh ở màn hình chat cho phép cấu hình được các trường tĩnh nếu chọn mẫu trả lời nhanh chat
- livechat v2- bật/tắt tính năng cho phép khách view lịch sử chat
- Chặn quyền merge phiếu ghi vào chính phiếu gốc
- Khách hàng có thể xem lịch sử chat livechat mà không cần bắt đầu phiên chat mới
- Ẩn thông báo agent tham gia chat từ màn hình view danh sách chat

#### 2. Fixed
- Bổ sung thông báo đang kết nối ở cschat khi đang kết nối websocket

### 02-12-2022

 #### 1. Added
- Nâng cấp bổ sung ghi nhận thêm comment vào phiếu ghi khi hành động auto trigger thực hiện
- Nâng cấp điều chỉnh thời gian check trung tin nhắn Facebook
- Nâng cấp hỏi xác nhận xóa phiếu ghi
- Bổ sung API tạo và update giá trị trường động Ticket và người dùng
- Tích hợp với Emas
- Giữ lại tính năng ẩn tạm thời boxchat trợ giúp
- Nâng cấp không merge phiếu ghi khi đang gọi

#### 2. Fixed
- Lỗi dữ liệu lệch khi xuất báo cáo chiến dịch chi tiết

## Tháng 11-2022

### 18-11-2022

 #### 1. Added
- Nâng cấp bổ sung thêm hành động chay bộ kích hoạt cho quy tắc tự động hóa
- Nâng cấp phần ghi nhận tên khách là emoji
- Tối ưu năng xuất thao tác của agent ở màn hình chi tiết tin nhắn
- Bổ sung thêm điều kiện lọc "Có giá trị" và "không có giá trị" với  trường kiểu ngày tháng ở filter phiếu ghi, quy tắc tự động hóa và bộ kích hoạt
- Bổ sung service_id vào api ticket và page_id vào api lịch sử chat facebook
- Nâng cấp điều kiện thời gian có giá trị trong các bộ lọc
- Nâng cấp ghi nhận ngày tháng trên ticket form


#### 2. Fixed
- Lỗi báo cáo thời gian thoại khi chọn khung giờ xuất ra so với biểu đồ
- Lỗi chuyển phiếu theo bộ phận nhưng không chuyển cho chuyên viên trong bộ phận đó mà giữ nguyên tại chuyên viên xử lý

### 04-11-2022

 #### 1. Added
- Nâng cấp bổ sung tính năng tin nhắn ice_breakers trên chatbot
- Nâng cấp lọc phiếu ghi theo chuyên viên/bộ phận tạo phiếu
- Nâng cấp ghi rõ cảnh báo lỗi, đính kèm hình ảnh khi nhận xét QA/phản hồi khiếu nại QA
- Nâng cấp điều chỉnh vị trí button livechat theo nhu cầu trên Livechat version 2
- Nâng cấp bổ sung thêm option khi nhúng chat, ẩn box chat khi user minimize ô chat
- Nâng cấp thêm phần ghi nhận lại lịch sử tương tác cho khách hàng trên livechat
- Nâng cấp thay đổi cơ chế check agent online định kỳ ở phần widget chat

#### 2. Fixed
- Lỗi hiển thị thiếu nội dung email trong phiếu ghi
- Lỗi zalo được đánh giá nhiều lần khi gửi qua trigger
- Lỗi tạo filter phiếu ghi chọn thời hạn xử lý
- Lỗi phần nghe lén khi nghe liên tục
- Lỗi đăng nhập smartdialer báo không có quyền
- Lỗi chọn tổ chức ở giao diện chat

## Tháng 10-2022

### 21-10-2022

 #### 1. Added
- Nâng cấp xuất báo cáo tìm kiếm nghe lại cuộc gọi bổ sung phân quyền nghe file ghi âm theo role
- Bổ sung 2 sự kiện merge phiếu ghi và xóa phiếu ghi trong webhook
- Bổ sung nguồn chi tiết phiếu ghi có giá trị gclick = True/False và cho phép filter/báo cáo
- Nâng cấp phân phối realtime chat bị lấy lại bởi trigger chatbot
- Nâng cấp cho phép bỏ chọn , xóa nội dung khi sử dụng macro và mẫu trả lời nhanh
- Nâng cấp thêm thuộc tính Google Click ID trong nguồn chi tiết

#### 2. Fixed

### 07-10-2022

 #### 1. Added
- Nâng cấp tạo kịch bản tin nhắn facebook cho phép tạo Button
- Bổ sung hành động notification trong bộ kích hoạt, quy tắc tự động hóa
- Bổ sung thêm điều kiện lọc "Có giá trị và không có giá trị" cho trường Người dùng trong Bộ lọc
- Nâng cấp  tin nhắn tự động - Bổ sung cho phép gửi không giới hạn
- Giao diện bổ sung loại nút bấm trong quản lý kịch bản Facebook
- Nâng cấp cơ chế load visitor
- Nâng cấp thu nhỏ lại ảnh
- Nâng cấp thông báo chi tiết mã lỗi dang text cho Agent khi gọi ra không thành công (408, 480,...) Áp dụng với tất cả nhà mạng việt nam và các nhà mạng nước ngoài
- Bổ sung báo cáo tỷ lệ không đạt target thời gian theo giai đoạn


#### 2. Fixed
- Fix lỗi khi hiển thị thuộc tính cha chon khi chọn gia trị mặc định trong ticket form
- Fix lỗi hiển thị thông tin trường trên livechat


## Tháng 09-2022

### 23-09-2022

 #### 1. Added
- Nâng cấp ghi nhận tin nhắn đã được phản hồi nếu chỉ gửi ảnh
- Nâng cấp cấu hình bỏ gom nhóm trong danh sách phiếu ghi
- Bổ sung báo cáo tỷ lệ phản hồi theo automation
- Nâng cấp thay đổi class khi action tác động vào trường động kiểu stage (7)
- Thêm trường tên  nhóm vào filter Danh sách khách hàng
- Bộ quy tắc doanh nghiệp: bỏ cấu hình thời gian thực hiện action ở các action khác ngoài email sms, facebook...
- Bổ sung thêm điều kiện lọc "Có giá trị và không có giá trị" cho trường Điểm đánh giá của bộ lọc phiếu ghi
- Bổ sung giao diện cấu hình khung thời gian chạy chiến dịch Smartdialer
- Bổ sung thêm cột tên Chuyên viên bị đánh giá trong báo cáo QA
- Nâng cấp API lấy được chi tiết tất cả cuộc gọi trong smartdialer
- Nâng cấp sort theo môc SLA và Target Spent Time trong danh sách phiếu ghi
- Bổ sung chỉ số báo cáo: Thời gian phản hồi tin nhắn trung bình trong báo cáo thời gian phản hồi của Facebook
- Tối ưu tốc độ tìm kiếm trong lịch sử chat
- Nâng cấp báo cáo inbox zalo
- Bổ sung báo cáo tỷ lệ đặt target spent time for stage
-Bổ sung thêm thông báo tin nhắn tự động tiếp nhận lần 2

#### 2. Fixed
- Fix lỗi khi ticket đang mở và được cập nhật trong lúc có hành động auto hoặc trigger vào thì hành động không được ghi nhận
- Fix lỗi giá trị thuộc tính đã nhập bị clear khi ấn đã xong ticket từ giao diện bình luận

## Tháng 08-2022

### 30-08-2022

 #### 1. Added
- Nâng cấp cơ chế phân phối phiếu ghi cuộc gọi nhỡ: phân phối phiếu ghi cuộc gọi nhỡ cho chuyên viên được phân phối cuối cùng
- Nâng cấp Trigger lấy lại chat sau 10 phút cho phép lấy luôn chat và phân phối lại cho Agent theo điều kiện cài đặt của trigger chatbot
- Nâng cấp ghi nhận kết quả action vào ticket comment khi sử dụng trigger (Bộ kích hoạt) và automation (Quy tắc tự động hóa)
- Nâng cấp thêm phần webhook cho trigger
- Bổ sung chỉ số "Thời gian phản hồi tin nhắn trung bình" trong báo cáo thời gian phản hồi của Facebook
- Nâng cấp API lấy list cuộc gọi smartdialer và trên CareSoft
- Bổ sung báo cáo "Tỷ lệ inbox bị thu hồi" trong báo cáo Facebook
- Bổ sung cho phép thiết lập trường động trong mẫu trả lời bình luận facebook
- Bổ sung cấu hình ẩn checkbox "Tự động trả lời" trên giao diện
- Nâng cấp báo cáo funnel: cho phép khi click sang xem dữ liệu hình bảng thì hiển thị số liệu Stayed từng giai đoạn của tiến trình

#### 2. Fixed

### 19-08-2022

 #### 1. Added
- Nâng cấp tắt bật chuyển solved về closed sau 24h cuối ngày theo domain
- Nâng cấp phần chatbot: Cho phép thêm 2 giá trị là Chọn 1 phương án và chọn nhiều phương án tại "Thiết lập thuộc tính"
- Bổ sung lưu log thay đổi trường động kiểu stage (tiến trình) khi tạo mới/cập nhật phiếu ghi:
    - API
    - Chat và bình luận
    - Tạo ticket từ chiến dịch import file
- Áp dụng lazy load với các thuộc tính lựa chọn chứa nhiều data:
    - Cụm bộ quy tắc doanh nghiệp: Cấu hình trigger, Cấu hình Automation, Cấu hình SLA
    - Bộ lọc phiếu ghi
    - Bộ cấu hình ticket Filter
    - Các filter báo cáo
    - Macro cho phiếu ghi
    - Màn hình phiếu ghi
    - Mẫu trả lời nhanh cho SMS
    - Màn hình gửi mới SMS, màn hình reply SMS
    - Các loại chiến dịch trong bộ lọc ticket
    - Tin nhắn tự động
    - Dịch vụ
    - QA Script
- Bổ sung cấu hình ẩn hiện trường Mốc SLA ở view ticket
- Bổ sung cấu hình spent time target (áp dụng cho trường động phiếu ghi kiểu tiến trình)
- Build tool cấu hình workflow
- Livechat - Bổ sung giao diện ở box đánh giá: bắt buộc phải có đánh giá mới cho submit
- Nâng cấp không ghi nhận nhỡ trong báo cáo với tin đồng bộ
- Bổ sung báo cáo Target spent time
- Báo cáo chiến dịch - bổ sung thêm thông tin thời gian bắt đầu cho những cuộc chưa thành công
- Nâng cấp phần hiển thị đánh giá livechat: khi đóng chat vẫn hiển thị ra để khách hàng thực hiện survey

#### 2. Fixed
- Fix lỗi phần chọn trạng trong bộ lọc phiếu ghi
- Fix điều kiện tìm kiếm bất đồng bộ ở bộ filter báo cáo với nguồn chi tiết

### 05-08-2022

 #### 1. Added
- Thêm Email bằng tài khoản Outlook
- Nâng cấp API Facebook
- Bổ sung param "send_to" cho các action bộ quy tắc doanh nghiệp
- Nâng cấp survey Livechat
    + đưa phần survey in chat xuống dưới box gửi tin nhắn, đưa menu lên trên
    + bỏ phần cấu hình survey livechat
    + bỏ Button gửi survey manual ở màn hình agent
    + bổ sung survey khi agent thoát chat (luồng giống như khi user thoát)
    + convert tin nhắn rate của khách về dạng tin nhắn bình thường (biểu tượng like dislike)
    + bỏ phần remove rating (khi rate xong bấm lại phần rate đó sẽ bỏ đánh giá)
    + bổ sung giao diện cấu hình nút rate (like dislike, start, emoticon), bổ sung giao diện rate ở widget
    + bổ sung không gửi lại survey nếu khách đã rate
- Bổ sung Action gửi notify hỗ trợ gửi đầy đủ: requester, assignee … trong bộ quy tắc doanh nghiệp
- Bổ sung tính năng chỉnh sửa font chữ cho toàn bộ đoạn văn bản
- Bổ sung phần tạo mới phần kịch bản cho các action trong bộ quy tắc doanh nghiệp
- Nâng cấp tối ưu về mẫu trả lời nhanh chat,commment (tạo được hơn 1000 mẫu)
- Bổ sung hành động thay đổi nhân viên xử lý trong trigger
- Bổ sung cho phép cấu hình ẩn dòng thông báo chuyển agent (các dòng hint tiêu cực) ở màn hình chat phía khách hàng trên Livechat
- Bổ sung button chuyển tới tin nhắn cuối cùng ở màn hình chat phía khách hàng

#### 2. Fixed
- Fixbug hiển thị Trường có nhiều lựa chọn khi set về giá trị mặc định hoặc lựa chọn không có giá trị trong bộ quy tắc doanh nghiệp
- Fixbug đóng phiếu ghi cha bị cập nhật vào phiếu ghi con

## Tháng 07-2022

### 22-07-2022

 #### 1. Added
- Bổ sung lịch cho các action của Trigger. [Chi tiết](https://kb.caresoft.vn/support/hc#/?type=article&id=5110)
- Nâng cấp ghi nhận thời gian phản hồi lần đầu phiếu ghi chat trong báo cáo thời gian phản hồi phiếu ghi
- Nâng cấp luồng Lead Nurturing. [Chi tiết](https://kb.caresoft.vn/support/hc#/?type=article&id=5111)
- Bổ sung đầy đủ thay thế tham số của Người yêu cầu, Chuyên viên và Ticket
- Update báo cáo Funnel: Bổ sung tính % tất cả các stage kết thúc(cơ chế cũ chỉ tính % chung)
- Bổ sung Livechat version 2. [Chi tiết](https://kb.caresoft.vn/support/hc#/?type=article&id=5112)

#### 2. Fixed
- Thay đổi chữ " Gán" thành chữ " Chuyển" khi để ngôn ngữ là Tiếng Việt trong tính năng Assign ticket

### 08-07-2022

 #### 1. Added
- Bổ sung lịch cho các hành động của bộ quy tắc doanh nghiệp: zalo, sms, email, facebook ...
- Bổ sung thêm trường đánh dấu ticket đã merge được merge:
    - Đánh dấu pri và second ticket trong tính năng merge ticket, thêm cả vào solr để tìm kiếm loại bỏ được
    - Trạng thái phiếu ghi để luôn là closed (hiện đang để là solved)
- Bổ sung phân quyền giao phiếu ghi QA cho vai trò Sub-admin
- Nâng cấp lưu lại view các trường trong Bộ lọc:
    - Đổi tên Bộ lọc khách hàng thành Bộ lọc, tiếng anh là Segment.
    - Lưu lại theo client khi người dùng thay đổi hiển thị các trường: đi theo các bộ lọc đã lưu trên hệ thống, các bộ lọc mới thì hiển thị theo mặc định.
- Bổ sung chức năng Reply ở Agent cập nhật ticket
- Nâng cấp thêm tính năng mở rộng - thu gọn các vùng  nhập liệu ticket


#### 2. Fixed
- Thay đổi cơ chế lựa chọn thuộc tính động ở báo cáo Funnel (dùng drilldown controll)

### 01-07-2022

 #### 1. Added
- Bổ sung điều kiện tìm dữ liệu phiếu ghi và người dùng theo **"NĂM HIỆN TẠI"** và **"THÁNG HIỆN TẠI"** đối với trường dữ liệu kiểu ngày tháng (chỉ áp dụng cho trường động)
- Nâng cấp bổ sung tính năng **"Gán phiếu ghi tới QA"** cho chuyên viên bất kỳ
- Bổ sung tag referal uri từ khách chat ẩn danh facebook trên website
- Nâng cấp action gọi ra bộ lọc. Cho phép truyền list phiếu ghi và gọi ra trên list phiếu ghi này
    - **_Web: Import từ file_**
    - **_Api: Thêm tickets arrays_**
- Bổ sung log đánh giá survey facebook vào phiếu ghi
- Điều chỉnh hiển thị tên Visitor thành tên User trong màn hình đang Chat
- Đẩy dữ liệu vào chiến dịch với chiến dịch và kịch bản đã tạo trước đó: Api và import - FrontEnd
- Nâng cấp hiển thị tương tác chuyên viên đã chuyển về End-user trong báo cáo

#### 2. Fixed
- Fix lỗi không gửi được chat trên iphone khi user suspend app
- Fix lỗi gửi file từ web Livechat ko gửi được nhưng không hiển thị thông báo lỗi

## Tháng 06-2022

### 22-06-2022

 #### 1. Added
- Bổ sung link phiếu ghi ở trường số phiếu ghi trong file xuất dữ liệu phiếu ghi. Áp dụng cho **xuất phiếu ghi trong bộ lọc phiếu ghi** và **xuất báo cáo lịch sử cuộc gọi**
- Bổ sung quyền, cấu hình chặn xem dữ liệu của agent khác trên giao diện phiếu ghi, thông tin khách hàng
- Bổ sung bộ lọc các nhà mạng trong báo cáo SMS
- Bổ sung quyền chặn không cho Agent chuyển phiếu ghi (_không áp dụng cho màn hình chuyển phiếu ghi trong bình luận Facebook_)
- Bổ sung phân quyền xem người dùng theo role (vai trò)
- Bổ sung account_config **LAST_AGENT_CHAT_CHECK_SYSTEM_STATUS**: cấu hình chat last agent có check đang có cuộc gọi không (0: Khong check; 1: Check). Default 0. Mặc định (cơ chế cũ): nếu last agent thì nếu có đang có cuộc gọi vẫn đổ chat về. Nếu set là 1: Nếu đang có cuộc gọi sẽ ko đổ chat về
- Báo cáo phiếu ghi theo SLAv2 bổ sung các chỉ số **"không đạt"**, **"không tính"** trên biểu đồ, thay đổi giao diện hiển thị các chỉ số **"đạt"**, **"không đạt"**, **"không tính"** trên bảng số liệu chi tiết


#### 2. Fixed
- Fix lỗi đăng nhập đăng xuất Smartdialer token bị hết hạn

### 10-06-2022

 #### 1. Added
- Bổ sung mẫu trả lời nhanh Chat loại xin thông tin số điện thoại. [Chi tiết](https://kb.caresoft.vn/support/hc#/?type=article&id=5024)
- Bổ sung mẫu trả lời nhanh Chat loại xin thông tin Email. [Chi tiết](https://kb.caresoft.vn/support/hc#/?type=article&id=5025)
- Bổ sung lưu nguồn chi tiết vào phiếu ghi những phiếu ghi cuộc gọi smartdialer. [Chi tiết](https://kb.caresoft.vn/support/hc#/?type=article&id=5026)
- Bổ sung hành động thay đổi giá trị trường ngày tháng trong bộ kích hoạt (Trigger). [Chi tiết](https://kb.caresoft.vn/support/hc#/?type=article&id=5027)
- Bổ sung cho phép copy-paste bảng biểu Excel khi gửi Email trên biêu tượng Email và reply Email trong phiếu ghi. [Chi tiết](https://kb.caresoft.vn/support/hc#/?type=article&id=5028)

#### 2. Fixed
- Fix bug không kết nối tới bot khi khách chat ngoài giờ case resume lại chat mất kết nối
- Fix bug phân phối chat đồng bộ Zalo tin nhắn Survey
- Điều chỉnh cơ chế chatbot (Cho phép xử lý keyword từ button label của bot khác + Cho phép phân phối lại agent từ kịch bản rẽ nhánh từ trigger)

### 03-06-2022

 #### 1. Added
- Bổ sung paste ảnh trực tiếp từ giao diện ticket. [Chi tiết](https://kb.caresoft.vn/support/hc#/?type=article&id=5017)
- Bổ sung khách hàng phản hồi tin nhắn và khách hàng đọc tin nhắn trong bộ lọc phiếu ghi. [Chi tiết](https://kb.caresoft.vn/support/hc#/?type=article&id=5019)
- Bổ sung thời điểm khách hàng gửi tin nhắn cuối trong bộ lọc người dùng. [Chi tiết](https://kb.caresoft.vn/support/hc#/?type=article&id=5020)
- Bổ sung thêm điều kiện "Tháng trong năm" trong Trigger LiveChat. [Chi tiết](https://kb.caresoft.vn/support/hc#/?type=article&id=5021)
- Bổ sung hành động gửi tin nhắn Facebook trong Bộ kích hoạt (Trigger) và Quy tắc tự động hóa (Automation). [Chi tiết](https://kb.caresoft.vn/support/hc#/?type=article&id=5022)

#### 2. Fixed
- Fix lỗi ẩn field trong ticketfilter khi reload lại hiển thị lại

## Tháng 05-2022

### 27-05-2022

 #### 1. Added
- Bổ sung API gửi ZNS. [Chi tiết](https://kb.caresoft.vn/support/hc#/?type=article&id=5008)
- Bổ sung API thêm hàng loạt khách hàng vào chiến dịch smartdialer ( theo contact_id, theo email, theo số điện thoại), mục 4.4, 4.5 và 4.6. [Chi tiết](https://kb.caresoft.vn/support/hc#/?type=article&id=2029)
- Nâng cấp cho phép tự mapping dữ liệu từ ticket vào template gửi ZNS khi gửi ZNS ra từ ticket. [Chi tiết](https://kb.caresoft.vn/support/hc#/?type=article&id=5010)
- Bổ sung Gửi Zalo ZNS cho hành động trigger (bộ kích hoạt) và Automation (quy tắc tự động hóa). [Chi tiết](https://kb.caresoft.vn/support/hc#/?type=article&id=5011)
- Bổ sung rating in chat trong phiên bản live chat mới (in-app)
- Update chức năng view ảnh (widget live chat v2)
- Update bộ script nhúng mới, support thêm tính năng + extendable, fix được lỗi nhúng chat 2 lần không mở được, support các event handler từ widget (Widget live chat)

#### 2. Fixed
- Fix lỗi tính sai SLAv2 case không có lịch khi pending phiếu

### 20-05-2022

 #### 1. Added
- Nâng cấp segmentTicket cho phép hiển thị SLA V2 theo ticket (khi gán quyền thì phần điều kiện sẽ tự ẩn điều kiện tìm theo Đáp ứng SLA V1) và tự bổ sung cột Đáp ứng SLA (V2)
- Thêm tỉ lệ % hiển thị ở biểu đồ tổng quan báo cáo ZNS và tỉ lệ Đã phản hồi
- Bộ lọc bổ sung 2 điều kiện Thời gian cuối cùng người dùng gửi tin nhắn Zalo / Thời gian cuối cùng người dùng gửi tin nhắn Facebook

#### 2. Fixed
- Fix lỗi SLA v2 phiếu ghi reopen bị tính sai

### 13-05-2022

 #### 1. Added
- Bổ sung gửi ZNS ra từ ticket 
- Bổ sung kiểu dữ liệu link trường động ticket
- Nâng cấp app của Yody và Gapo
- Bổ sung điều kiện chọn tìm không nằm trong 1 bộ lọc ( Tìm khách hàng không thuộc 1 bộ lọc ticket đã cấu hình trước)
- Zalo ZNS bổ sung cơ chế ghép người dùng Zalo khi chat lại vào người dùng nhận tin ZNS

#### 2. Fixed
- Fix lỗi update kịch bản IVR - không update trạng thái dịch vụ voicemail

### 06-05-2022

 #### 1. Fixed
- Fix lỗi báo cáo tổng quan smartdialer sai mã lỗi (Đếm thiếu các mã khác 200 và 468)


## Tháng 04-2022

### 29-04-2022

 #### 1. Added
- Bổ sung tính năng gửi tin nhắn Zalo ZNS riêng lẻ 
- Bổ sung account_config MONITOR_WAITING_CALL_SEC_CONFIG: Cấu hình giám sát các cuộc gọi thời gian chờ nhỏ hơn số giây cấu hình (Default 60)

### 22-04-2022

 #### 1. Added
- Bổ sung app Yody    
- Nâng cấp app của Pharmacity - thay key của token 
- Nâng cấp app của gapo, cho phép tìm khách theo cả email 
- Bổ sung tự động gắn tag tên quảng cáo fb vào phiếu ghi chat
- Bổ sung thời điểm đọc tin nhắn Zalo

#### 2. Fixed
- Lỗi bị lệch thời gian đàm thoại với thời gian ở file ghi âm của cuộc gọi ra khi bị nghe lén
- Fix lỗi chọn trường số điện thoại trong ticketfilter ra không ẩn số khi được gán quyền

## 16-04-2022

 #### 1. Added
-   Bổ sung option đánh giá lại cho survey kênh thoại: trước đây mặc định luôn đánh giá lại, kể cả khách không bấm survey, bổ sung thêm không bấm gì thì không update
- Nâng cấp bổ sung thay đổi ngôn ngữ cho agent trên giao diện CareSoft
- Nâng cấp merge người dùng cho tùy chọn đổi hướng
- Điều chỉnh cơ chế login lại sau 20s trên caresoft với api sms mobifone
- Bổ sung account_config LAST_AGENT_CHAT_CHECK_MAX_CHAT bằng cấu hình phân phối chat last agent có check điều kiện max chat (0: Không check; 1: Check). Default 0


### 08-04-2022

 #### 1. Added
- Nâng cấp cấu hình cho phép chặn không cho chấm điểm lại phiếu ghi QA: [Chi tiết](https://web22.caresoft.vn/support#/index?type=selectNav&id=helpcenter&nav=article&articleId=4947)   
- Bổ sung APP trong ticket 
- Bỏ tự động thêm kỹ năng cho nhóm của admin khi thêm mới/kích hoạt dịch vụ

#### 2. Fixed
- Fix lỗi case chatbot phân phối vào danh sách agent chưa hoạt động theo cấu hình last agent của account_config
- Lỗi ghi nhận trạng thái nhỡ tại chuyên viên trong báo cáo số cuộc gọi nhỡ (Khi bấm vào chi tiết thì đang ghi nhận tất cả là từ chối)
- Thuộc tính với kiểu dữ liệu bằng tiến trình không hiển thị text ở giao diện báo cáo
- Lỗi thỉnh thoảng gọi ra phiếu ghi cũ lại tạo phiếu ghi mới 

### 01-04-2022

 #### 1. Added
- Bổ sung API lấy chi tiết nguồn ticket và hiển thị ID nguồn chi tiết trong ticket: [Chi tiết](https://kb.caresoft.vn/support/hc#/?type=article&id=24)
- Bổ sung hiển thị ad_id, campaign_id… trong cấu hình nguồn chi tiết: [Chi tiết](https://kb.caresoft.vn/support/hc#/?type=article&id=24)
- Nâng cấp bỏ disabled thời gian đồng bộ comment, inbox khi update page facebook cấu hình
- Bổ sung mapping đồng bộ thuộc tính Chi Nhánh, tổng đơn hàng vào trường động ticket, mapping ngày sinh nhật, mã khách hàng vào trường động Người dùng. Phát sinh thay đổi tạo ticket từ API sẽ trả về thêm requester_id
   + App config ticket addition fields mapping: orderCode, purchaseDate, branchName, orderTotal
   + App config user addition fields mapping: email, phoneNo, birthday, code
- Thêm campaign_id trong nguồn chi tiết ticket từ ads facebook
- Thêm điều kiện lọc trạng thái phân phối acd trên trigger chatbot
- Thêm config auto gán nhóm nguồn trong tên chiến dịch quảng cáo Facebook

#### 2. Fixed
- Fix lỗi cấu hình điều kiện SLA null khi chọn giá trị “không bằng” trong các điều kiện 

## Tháng 03-2022

### 25-03-2022

 #### 1. Added
-  Bổ sung 2 account_config cho phần gọi vào bấm máy lẻ IVR: [Chi tiết](https://kb.caresoft.vn/support/hc#/?type=article&id=4930)
    + CALL_IVR_EXT_BUSY_MUSIC: đường dẫn file ghi âm sẽ phát khi timeout mà số máy lẻ bận/không nghe máy
    + CALL_IVR_EXT_INVALID_MUSIC: đường dẫn file ghi âm sẽ phát khi bấm số máy lẻ không đúng (số máy lẻ không tồn tại/số máy lẻ ở trên domain khác)
-  Nâng cấp lưu log chất lương cuộc gọi theo từng cuộc gọi. [Chi tiết](https://kb.caresoft.vn/support/hc#/?type=article&id=4931)
-  Nâng cấp hành động chiến dịch bổ sung hành động  Gửi tin nhắn OTN. [Chi tiết](https://kb.caresoft.vn/support/hc#/?type=article&id=4928)

#### 2. Fixed
-  Fix lỗi khi gọi vào IVR bấm trực tiếp số máy lẻ nhưng số máy lẻ đó thuộc domain khác thì bị gán sang agent của domain khác
-  Fix lỗi “Bấm đã xong chat không lưu thông tin trường động với live chat mà khách đã thoát"

### 18-03-2022

 #### 1. Added
-  Chatbot thêm tính năng gửi đăng ký OTN (One-Time Notification). [Chi tiết](https://kb.caresoft.vn/support/hc#/?type=article&id=4927)
    - Thêm màn hình quản lý chủ đề OTN (One-Time Notification)
    - Bổ sung loại thẻ gửi đăng ký OTN (One-Time Notification)
    - Facebook Login bổ sung đăng ký nhận event messaging_optins (event token khi khách bấm nhận thông báo OTN)
    - Lưu danh sách người dùng Facebook đăng ký OTN Topic
-  Nâng cấp cho phép gửi email reply (tạo ticket mới) đối với ticket close  có gắn quyền không được phép cập nhật lại. [Chi tiêt](https://kb.caresoft.vn/support/hc#/?type=article&id=4925)
-  Nâng cấp khi ticket close mà nhân bản thì ticket mới sẽ ở dạng new. [Chi tiết](https://kb.caresoft.vn/support/hc#/?type=article&id=4926)
-  Bổ sung trường id phiếu ghi cha khi xuất phiếu ghi ra file
-  Zalo thêm cấu hình bật chế độ gửi tin chủ động cho domain
-  Báo cáo phiếu ghi xử lí với 1 cuộc gọi bổ sung tỉ lệ giải quyết với 1 cuộc gọi so với tổng phiếu ghi đã giải quyết

 #### 2. Fixed
-  Fix lỗi user chat live chat xong đóng chat, agent sau đó đóng hội thoại thì sẽ pass qua được phần validate phiếu ghi
-  Fix lỗi điều kiện có giá trị người xử lý trong trigger và các điều kiện thời gian

### 11-03-2022

 #### 1. Added
-  Bổ sung tính năng quản lý quảng cáo Facebook (Tên quảng cáo, Tên chiến dịch quảng cáo, ID tài khoản quảng cáo)
    - Chatbot thêm lọc điều kiện rẽ nhánh kịch bản theo (Tên quảng cáo, Tên chiến dịch quảng cáo, ID tài khoản quảng cáo)
    - Chat/ Nội dung chat trong ticket system message thêm thông tin (Tên quảng cáo, Tên chiến dịch quảng cáo, ID tài khoản quảng cáo)
    - Facebook thêm tính năng định kỳ check lại giấy phép của page (granted permissions) phục vụ chỉ gọi API Marketing khi đã có quyền
-  Bổ sung cấu hình account config nếu tiêu đề email khác với tiêu đề ticket thì tạo ticket mới EMAIL_CHANGE_SUBJECT_CREATE_NEW_TICKET Cấu hình tạo ticket mới nếu chủ đề email thay đổi so với chủ đề ticket cũ: [Chi tiết](https://kb.caresoft.vn/support/hc#/?type=article&id=4935)
-  Hiển thị chi tiết thông tin cuộc gọi của của nhân viên bị chuyển vai trò thành enduser trên báo cáo

 #### 2. Fixed
-  Fix lỗi khi đang offline thoại mà chuyển thiết bị nghe gọi tự online thoại lên nhưng trạng thái chung vẫn off. Thay đổi cơ chế trạng thái chung sẽ off nếu bị no answer (cũ là khi no answer thì vẫn online trạng thái chung)
-  Fix lỗi hiển thị thời gian trạng thái noanswer trong báo cáo chuyên viên

### 04-03-2022

 #### 1. Added
-  Nâng cấp gửi email ra bỏ link blogspot trong ảnh
-  Nâng cấp xem chi tiết email có BCC, File đính kèm
-  Bổ sung cấu hình hình ảnh trong nội dung email gửi theo kiểu inline attach  (chỉ cho giao diện editor gửi ra)
-  Bổ sung cấu hình không reopen lại closed ticket (khong duoc update closed ticket): [Chi tiết](https://kb.caresoft.vn/support/hc#/?type=article&id=4937)
-  Sửa lại login Facebook. Luồng thêm mới lại page -> Login lại (Gán quyền thực hiện trên của sổ đăng nhập Facebook). Sau khi gán quyền sẽ tự động thêm mới hàng loạt các page được gán nếu chưa tồn tại hoặc chưa active trên domain. (Bỏ cơ chế chọn từng page)
-  Chatbot tính năng Rẽ nhánh kịch bản bổ sung điều kiện lọc theo tên quảng cáo

 #### 2. Fixed
-  Fix lỗi Automation không nhận CCs, Followers
-  Fix lỗi Automation không nhận điều kiện không bằng (khi dữ liệu bị null)
-  Bộ quy tắc doanh nghiệp: Fix lỗi action gửi email khi không chọn add comment thì không chọn được dịch vụ email
-  Fix lỗi báo cáo cuộc gọi vào bị lệch chỉ số giữa bảng và chi tiết khi xem ở row không có bộ phận

## Tháng 02-2022

### 25-02-2022

 #### 1. Added
-  Nâng cấp Zalo API Login
-  Nâng cấp bộ lọc người dùng bổ sung tìm theo Zalo ID
-  Chatbot filter theo ads thêm nhiều điều kiện (postId / adId)

 #### 2. Fixed
-  Fix lỗi email gửi ra reply lỗi base href khiến không gửi được
-  Fix email gửi ra tên file đính kèm lấy đúng tên file gốc up lên
-  Fix lỗi báo cáo thời gian chuyển đổi bị lệch cột tổng thời gian và trung bình

### 19-02-2022

 #### 1. Added
-  Bổ sung cấu hình account_config max attach size của file khi load email về: [Chi tiết](https://kb.caresoft.vn/support/hc#/?type=article&id=4939) 
-  Bổ sung cấu hình account_config chọn tên người dùng khi merge người dùng: [Chi tiết](https://kb.caresoft.vn/support/hc#/?type=article&id=4941) 
-  Chatbot bổ sung thẻ lưu thuộc tính
-  Nâng cấp bổ sung đồng bộ tin nhắn auto khi KH follow OA

 #### 2. Fixed
-  Fix bổ sung index khi thêm cc và follow ticket quan trigger và automation trong bộ quy tắc doanh nghiệp
-  Fix thỉnh thoảng có email đính kèm file csv bị parse thành nội dung text

### 11-02-2022

 #### 1. Added
-  Nâng cấp tính năng cho sắp xếp vị trí các lựa chọn trong trường động người dùng (kiểu dữ liệu chọn 1 và chọn nhiều)
-  Webhook ticket tạo từ web bổ sung action phân biệt create và update
-  Chatbot bổ sung quản lý thuộc tính động cho hội thoại
-  Nâng cấp ui libs version Date Time Picker:
    -  CRUD thuộc tính động hội thoại
    -  Lưu giá trị vào trường thuộc tính động hội thoại ở các thẻ hỏi thông tin từ người dùng , thẻ phản hồi nhanh
    -  Bổ sung chèn tham số thuộc tính động hội thoại ở thẻ tin nhắn Văn bản
    -  Bổ sung chèn tham số thuộc tính động hội thoại ở thẻ JSON API
    -  Bổ sung điều kiện lọc theo thuộc tính động hội thoại ở thẻ chuyển hướng.
-  Bổ sung hỗ trợ kiểu trường động phiếu ghi kiểu Stage khi export ticket
-  Nâng cấp hiển thị số điện thoại phụ trong báo cáo chiến dịch khi không có số điện thoại chính
-  Bổ sung sorting trong báo cáo agent, 2 bảng Thời gian trạng thái theo kênh, Lich sử thay đổi trạng thái
-  Cschat quản lý visitor bổ sung interval update thông tin visitor mỗi 1 phút
-  Nâng cấp phân phối chat click vào bài viết quảng cáo theo danh sách agent cấu hình trong quản lý bài viết
-  Bổ sung điều kiện khoảng giờ cho Trigger

 #### 2. Fixed
-  Fix lỗi hiển thị mất chọn agent trong ticket khi view ticket
-  Fix lỗi giao diện tạo hành động trigger và automation của Follower và Ccs
-  Fix lỗi bị lẫn avatar khi chuyển đổi chat giữa các khách hàng chat qua zalo
-  Fix lỗi xuất dữ liệu ở báo cáo lịch sử cuộc gọi không ra đúng số lượng theo báo cáo
-  Fix lỗi lệch dữ liệu khi xuất từ biểu đồ trong báo cáo
-  Fix lỗi gọi ra trong tiket nhưng lại tạo thành ticket mới
-  Fix lỗi lọc báo cáo khách hàng ( theo tương tác) khi thêm điều kiện dịch vụ
-  Fix bug phân phối lại chat theo danh sách agent bị phụ thuộc vào trạng thái online của các agent có kỹ năng
-  Fix lỗi mất kết nối thoại khi user chưa đăng nhập hoặc đăng xuất xong đăng nhập lại
-  Fix lỗi xử lý phiếu ghi hàng loạt bị update đè kết quả kịch bản chiến dịch

## Tháng 01-2022

### 14-01-2022

 #### 1. Added
-  Bổ sung ẩn các nút survey Facebook tránh agent click lại bằng cách xem lại lịch sử chat
-  Nâng cấp hiệu năng và mở lại phần đồng bộ realtime tin nhắn OA gửi ra
-  Zalo nâng cấp hiển thị text của button chatbot
-  Bổ sung giao diện hiển thị ping cuộc gọi, thông tin chất lượng cuộc gọi gần nhất
-  Điều chỉnh bổ sung trạng thái kết thúc trong báo cáo với những cuộc gọi ra lỗi kết nối thiết bị nghe gọi (Hiển thị thành những cuộc gọi bị lỗi ở trạng thái kết thúc)
-  Bổ sung thêm account id vào đường link load avatar facebook

 #### 2. Fixed
-  Fix bug Zalo tạo 2 user khi chat và từ chưa follow sau đó follows
-  Fix bug sai số lượng giám sát chat
-  Fix lỗi hiển thị avatar của user facebook chặn Page (Hiển thị avatar mặc định thay vì ảnh lỗi)
-  Fix lỗi thỉnh thoảng gọi ra không lưu ticket khi gọi ra giữa các cuộc quá nhanh
-  Fix lỗi hiển thị sai tên email khi transfer, trao đổi nội bộ mà người bị chuyển từ chối
-  Fix lỗi không hiển thị tên page trong lịch sử facebook
-  Fix lỗi từ báo cáo lịch sử cuộc gọi nhập số điện thoại ở bộ lọc chuyển sang báo cáo chi tiết hiển thị sai dữ liệu
-  Fix lỗi kẹt tooltip hiển thị nội dung phiếu ghi ở 1 số trường hợp
-  Fix lỗi giao diện bị xô khi danh sách các tab đang dài và tiếp nhận cuộc gọi tới
-  Fix lỗi sorting bảng ở báo cáo lịch sử cuộc gọi sau đó chuyển sang báo cáo ivr chi tiết hiển thị có lỗi xảy ra 

### 07-01-2022

 #### 1. Added
-  Bổ sung cơ chế chuyển tab mới: định danh theo tab id
-  Nâng cấp thay đổi điều kiện lấy thông tin số điện thoại trên báo cáo khách hàng theo tương tác
-  Thay đổi giao diện check phiếu ghi của người dùng
-  Bổ sung validate chặn đóng chat từ server khi agent chưa phản hồi chat
-  Bổ sung cấu hình timeout tạo chat nhỡ khi phân phối theo danh sách agent
-  Update không tạo ticket từ tin nhắn đồng bộ
-  Update không cho đổi tên người dùng nếu agent chat vào livechat
-  Bổ sung cấu hình bật tắt lưu thời gian tạo ticket = thời gian hội thoại khởi tạo

 #### 2. Fixed
-  Fix lỗi supervisor không view được lịch sử chat realtime
-  Fix lỗi Sửa lại cơ chế các báo cáo Facebook
-  Fix lỗi phát file ghi âm không khớp trong báo cáo lịch sử cuộc gọi và báo cáo chiến dịch (chi tiết và Tổng quan) - Đổi cơ chế chỉ load file khi bấm nút play
-  Fix lỗi chồng đè thông tin cuộc gọi vào và ra ở màn hình monitor khi tín hiện web-socket bị chập chờn
-  Fix lỗi agent đã đăng nhập không search được nội dung trên KB

# Năm 2021

## Tháng 12-2021

### 30-12-2021

 #### 1. Added
-  Bổ sung Thêm quyền ẩn/hiện chỉnh sửa trường động người dùng
-  Bổ sung ẩn chế độ phát lặp trong cấu hình IVR
-  Bổ sung cho phép chèn link vào ảnh trên chữ ký email và soạn thảo email

 #### 2. Fixed
-  Fix lỗi một số vị trí giao diện smart dialer  ( Tạo kịch bản Audio), Sửa lỗi IVR không lưu được file, Chỉ chọn chế độ chạy audio theo thời gian đối với nhánh IVR gặp agent
-  Fix lỗi không import được file âm thanh trong kịch bản smartdialer
-  Fix lỗi không chọn kỹ năng với kịch bản IVR đã xóa
-  Fix lỗi không tìm điều kiện trường động dạng số =0 ở bộ lọc

### 23-12-2021

 #### 1. Added
-  Tối ưu cấu hình TicketFilter, Cho phép kéo thả lưu luôn vị trí, lưu luôn trạng thái ẩn, Cho phép cấu hình quyền, cột dữ liệu đối với các filter mặc định
-  Bổ sung chặn không cho thay đổi người yêu cầu khi tương tác trong phiếu ghi, gán thêm phiếu ghi được CC khi tương tác trog phiếu ghi
-  Bổ sung con số tổng ở biểu đồ báo cáo khách hàng tương tác theo kênh
-  Bổ sung xóa dữ liệu trường ngày tháng khi dữ liệu macro kiểu ngày tháng rỗng
-  Chat zalo bổ sung retry khi api gửi ảnh lỗi
-  Bổ sung cơ chế tìm kiếm mới trong tìm kiếm CC, Follower, người yêu cầu Ticket, tìm kiếm khách hàng trong bộ lọc khách hàng các báo cáo
-  Bổ sung cảnh báo khi gửi tin nhắn zalo không thành công

 #### 2. Fixed
-  Fix Lỗi xóa trường động dạng ngày tháng năm: cập nhật phiếu ghi reload lại vẫn hiển thị giá trị cũ
-  Fix Lỗi ghi nhận tình trạng gọi chat của phiếu ghi khi gọi ra luôn là nhỡ mặc dù khách đã nhấc máy
-  Fix mất đồng bộ số lượng chat đang giữ giữa giao diện và hệ thống phân phối
-  Fix lỗi gửi file ảnh qua Zalo OA
-  Fix lỗi đã ngắt cuộc gọi trên IPPHONE nhưng trên giao diện không ngắt cuộc gọi
-  Fix lỗi validate macro kiểu dữ liệu Date không validate empty
-  Fix lỗi ghi nhận báo cáo nhỡ đồng bộ zalo (nâng cấp cơ chế đồng bộ giống facebook)
-  Fix lỗi bộ đếm lịch sử chat (3 kênh, bình luận, tin nhắn offline) hiển thị tất cả những chat chưa xử lí của những page mà agent không được gán kỹ năng nếu bật quyền xem lịch sử chat theo kỹ năng

### 17-12-2021

 #### 1. Added
-  Bổ sung bắt buộc 1 trường động khác theo giá trị chọn 1 của trường lựa chọn
-  Thay đổi cơ chế gọi ra từ phiếu ko đổi người yêu cầu theo user bị gọi ra, bổ sung cc user bị gọi ra nếu user bị gọi ra là user khác người yêu cầu
-  Bổ sung SLAv2 ghi nhận lại agent chịu trách nhiệm khi đóng SLA
-  Bổ sung log lỗi kết nối thoại
-  Click to call bổ sung trả về queryString trong gói tin window message
-  Báo cáo ticket slav2 sửa cơ chế tính sla cho agent, sửa cơ chế lọc theo thời gian tạo ticket

 #### 2. Fixed
-  Fix lỗi Gửi SMS ra từ phiếu ghi bị ghi đè nguồn phiếu ghi cũ
-  Fix lỗi giao diện/ ngôn ngữ một số điểm
-  Fix lỗi không merge ticket chat offline
-  Fix lỗi không lấy đầy đủ nội dung khi gửi email từ mail trên điện thoại iphone có đính kèm và chữ ký
-  Fix lỗi không nhận được cuộc gọi vào sau khi thực hiện gọi ra bấm trực tiếp trên ipphone nhưng không có quyền gọi ra
-  Fix bug import ticket không lưu nguồn phiếu ghi
-  Fix lỗi lịch vào cuối tuần bị tính sai SLA ở cấu hình lịch làm việc
-  Fix lỗi không lọc được các bình luận đã xong
-  Fix lỗi lọc comment phản hồi ở lịch sử bị lỗi
-  Fix bug báo cáo lấy số điện thoại theo user, bổ sung lưu người lấy số điện thoại vào user extension khi khách cho số điện thoại trước khi gặp agent

### 10-12-2021

 #### 1. Added
-  SLA2 bổ sung thêm để phù hợp thực tế: tính cho nội bộ, clone ticket, reopen, trường hợp ticket cuộc gọi gặp, chat gặp
-  Bổ sung đánh dấu trạng thái is lead cho ticket tạo từ api và ticket form
-  Bổ sung cấu hình gửi ảnh facebook dạng file trên account config (186 FACEBOOK_ALWAYS_SEND_IMAGE_AS_FILE = 1)
-  Bổ sung tìm ticket theo thời điểm gửi khảo sát trong bộ lọc ticket
-  Bổ sung tìm ticket theo ID phiếu ghi cha, Tìm phiếu ghi theo người cập nhật mới nhất, Thời điểm chuyển phiếu
-  Bổ sung trigger chatbot -  ghi nhận trạng thái agent đã phản hồi nếu phản hồi bằng file
-  Bổ sung câu lệnh hiển thị lịch sử chat facebook
-  Bổ sung Chatbot/comment -  sắp xếp độ ưu tiên rule trả lời cmt theo từ khóa, thêm loại gửi inbox bình luận dạng slide

 #### 2. Fixed
-  Fix giao diện SLA validate không điền giá trị 00 vào thuộc tính nếu điền 00 → Điền null
-  Fix lỗi api gửi ảnh facebook bị tự động crop size dẫn đến mất nội dung
-  Fix Bộ quy tắc doanh nghiệp lỗi solved phiếu ghi không update trường solved_time -> ảnh hưởng báo cáo
-  BzHour Fix lỗi tính sla theo lịch có ca vắt
-  Fix lỗi chiến dịch gửi sms không replace được tham số trường động người dùng dạng chọn 1, chọn nhiều
-  Fix lỗi gọi ra  gọi ra bị double click nên gọi ra 2 cuộc cùng lúc thành ra trạng thái bị lỗi
-  Fix lỗi không chèn ảnh vào template email và bổ sung tiêu đề khi chọn template email
-  Fix lỗi không chuyển được chat giữa các chuyên viên khi tiếp nhận trên mobile
-  Fix lỗi hiển thị trường bắt buộc trên ticket và user (Có dấu * màu đỏ)
-  Fix lỗi báo lỗi kết nối mạng
-  Fix lỗi báo cáo inbox lấy số điện thoại bị lệch biểu đồ và chi tiết
-  Fix lỗi phiếu ghi bị mất thông tin đã cập nhật
-  Sửa lại cơ chế các báo cáo Facebook
-  Fix lỗi không hợp nhất được nhiều phiếu ghi cùng lúc
-  Fix lại giao diện báo cáo version 2
-  Fix lỗi trùng keyword bảng 'groups' khi upgrade Mysql lên version 8.0




### 01-12-2021

 #### 1. Added
-  Bổ sung cơ chế tính toán các chỉ số trong SLA version 2
-  Bổ sung lưu thời gian lấy số điện thoại, email vào bảng conversation
-  Bổ sung lưu dữ liệu is_lead ticket thoại, livechat,facebook,zalo, comment facebook, facebook lead ads theo tương tác đầu tiên trong ngày trên 1 nguồn chi tiết
-  Bổ sung apps view thông tin bảo hành thông tin khách hàng từ ERP lên ticket
-  Bổ sung filter "Lọc Lead trùng" vào các báo cáo phiếu ghi
-  Api tạo ticket bổ sung thêm tham số is_public, không truyền thì mặc định là 1

 #### 2. Fixed
-  Fix lỗi báo cáo top khách hàng gọi ra không hiển thị dữ liệu theo nguồn
-  Fix lỗi chuyển chat đang hiển thị sai chuyên viên
-  Fix lỗi Chat/ Chat offline không search được theo tên người chat vào
-  Fix hiển thị thay đổi ngôn ngữ về tiếng việt ở giao diện Chat/ comment facebook/ Thông tin khách hàng / Thông tin thêm
-  Fix lỗi báo cáo thời gian phản hồi và giải quyết đang bị lệch dữ liệu biểu đồ và chi tiết
-  Fix lỗi không hiển thị SLA khi bật lịch làm việc
-  Fix bug sắp xếp tin nhắn đồng bộ facebook
-  Fix lỗi báo cáo thời gian đàm thoại đang ghi nhận cả cuộc gọi nhỡ

## Tháng 11-2021

### 26-11-2021

 #### 1. Added
-  Bổ sung  Segment Nhóm nguồn, Người cập nhật mới nhất lên bộ lọc ticket
-  Bổ sung cột ID phiếu ghi khi xuất phiếu ghi từ bộ lọc ticket
-  Bổ sung chặn cuộc gọi ra theo danh sách trong DB
-  Facebook bổ sung subscribe event messaging_policy_enforcement nhận các cảnh báo warning, block or unblock khi spam message
-  Bổ sung dữ liệu lấy sđt cho chat agent
-  Bổ sung webhook event create/update ticket từ nguồn api
-  Bổ sung watermark lưu thời gian đã xem tin nhắn Facebook
-  Bổ sung retry gửi tin nhắn Zalo ảnh khi api gửi ảnh zalo lỗi
-  Bổ sung báo cáo khách hàng tương tác theo kênh và báo cáo mới thêm các filter theo nguồn , nhóm nguồn, nguồn chi tiết , theo trường động phiếu ghi và tính lượng tương tác base theo ticket
-  Báo cáo phiếu ghi bổ sung filter trường động phiếu ghi cho tất cả các báo cáo
-  Báo cáo khách hàng bổ sung filter trường động khách hàng cho tất cả các báo cáo
-  Bổ sung bộ đếm slav2 trên danh sách phiếu ghi
-  Bổ sung hiển thị thời gian theo giờ:phút:giây trong lịch sử chat facebook/zalo/livechat
-  Bổ sung thời gian lưu thời gian cuối cùng assign của phiếu ghi

 #### 2. Fixed
-  Fix lỗi chuyển lại trạng thái phiếu ghi bình luận về chưa bỏ qua nếu khách comment lại
-  Fix lỗi phải đóng booking mới tiếp nhận được cuộc gọi trên app ứng dụng bên thứ 3
-  Fix lỗi email khi chuyển cuộc gọi đến máy lẻ bị sai
-  Fix bug gán sai nguồn chi tiết ticket tạo từ api không truyền tham số UTM
-  Fix bug xử lý tạo trùng người dùng facebook từ facebook ads
-  Fix lỗi bị mất thông tin trường thuộc tính khi cập nhật phiếu ghi
-  Fix lỗi cuộc gọi bị ghi nhận nhầm trong ticket của khách hàng khác
-  Fix lỗi hiển thị - avatar livechat không cập nhật sau khi thay đổi hoặc tự back lại hình cũ sau khi reload trình duyệt
-  Fix không lưu lại thông tin landing page trong báo cáo lịch sử chat
-  Fix lỗi báo cáo chiến dịch hiển thị dữ liệu bị lệch so với khi xuất file


### 19-11-2021

 #### 1. Added
-  Thay đổi label filter lịch sử chat và phần hiển thị người xử lí trong lịch sử chat ở 3 kênh Livechat, Facebook, Zalo
-  Thêm cấu hình validate bắt buộc đóng chat trước khi đăng xuất phần mềm


 #### 2. Fixed
-  Fix lỗi chuyển lại trạng thái phiếu ghi bình luận về chưa bỏ qua nếu khách comment lại
-  Fix bug lưu nguồn Người dùng Facebook thành Inbox Facebook nếu khách chat vào lần đầu tiên, cơ chế cũ đang lưu là nguồn Facebook
-  Fix bug clone chatbot không clone kịch bản Triggers
-  Fix lỗi giao diện giám sát Agent màn hình bị đen khi mở ra thông tin agent xong đóng lại
-  Fix lỗi cấu hình ticket filter, tạo trường động chọn 1, chọn điều kiện trường động chọn 1, xong lại đổi trường động đó về kiểu date
-  Fix lỗi thêm mới bot click vào bị undefined

### 17-11-2021

 #### 1. Added
-  Bổ sung chỉ số chat được phân phối lại vào báo cáo chat
-  Bổ sung lưu trữ thêm trường đánh dấu cuộc gọi trong báo cáo thoại
-  Bổ sung điều kiện lọc thời gian phản hồi trong báo cáo phiếu ghi - thời gian phản hồi lần đầu trung bình
-  Lịch sử chat Zalo bổ sung tin nhắn của bot (bao gồm các button và gallery ảnh) giống giao diện lịch sử facebook
-  Bổ sung App AMIS
-  Bổ sung cấu hình hiển thị số điện thoại, số điện thoại 2, số điện thoại 3 của người yêu cầu trong cấu hình danh sách ticket
-  Giao diện tin nhắn đang chat bổ sung hiển thị người đã gửi tin nhắn ra
-  Tối ưu  tốc độ báo cáo tổng quan-> khách hàng được tạo theo nguồn và Báo cáo Phiếu ghi -> Phiếu ghi đánh giá

 #### 2. Fixed
-  Fix bug xoá chat bot
-  Chatbot fix bug trigger với điều kiện Agent chưa phản hồi sẽ tự kích hoạt lại sau khi có agent mới tham gia chat
-  Chat fix bug không reload avatar của agent sau khi agent login lại
-  Chatbot fix bug không replace tham số người dùng trong kịch bản gửi tin nhắn trigger
-  Fix lỗi không xem được lịch sử chat
-  Fix lỗi kết nối trên phần mềm
-  Fix lỗi mặc định timeout cho các API từ bên thứ 3
-  Fix lỗi trường động phiếu ghi bắt buộc dạng văn bản có thể xóa được nội dung mà vẫn pass validate

### 12-11-2021

 #### 1. Added
-  Bổ sung yêu cầu thêm ticketcomment đầu tiên khi tạo booking/Lead, Validate trường động lead chọn mới dược tạo lead/booking
-  Bổ sung thêm title cho mẫu trả lời email
-  Nâng cấp version livechat, setup bắt buộc điền thông tin theo khung thời gian
-  Bổ sung hiển thị bỏ qua khi comment được comment vào tiếp, thay đổi bộ đếm bình luận
-  Bổ sung báo cáo Chat tách rõ 2 chỉ số: tiếp nhận và có phản hồi riêng ra ở các chart, tổng và chi tiết
-  Bổ sung lưu trữ thêm trường đánh dấu cuộc gọi trong báo cáo thoại
-  Update lại báo cáo các tin nhắn chưa xử lý trong báo cáo chat
-  Bổ sung tính năng transfer/trao đổi nội bộ hỗ trợ với cuộc gọi IVR bấm trực tiếp máy lẻ đến đích là divert di động
-  Bổ sung tự lấy ngày đặt lịch từ ticket lên giao diện booking
-  Bổ sung cấu hình trạng thái (account_config) khi agent đăng nhập
-  Bổ sung cho phép phân phối lại chat chưa phản hồi khi có agent đang giữ chat theo điều kiện cấu hình trigger chatbot
-  Bổ sung cấu hình trạng thái ticket cho thẻ đóng chat tạo ticket chatbot
-  Zalo bổ sung convert tin nhắn bấm nút từ bot bên thứ 3 cho dễ nhìn
-  Bổ sung cấu hình gửi sms từ phiếu ghi không ghi đè chủ đề
-  Bổ sung quyền Ẩn trường giới tính
-  Bổ sung quyền chuyển role trong phần cấu hình chuyên viên: tránh user không phải admin tự chuyển mình thành admin
-  Bổ sung chatbot auto chuyển chat cho người khác theo điều kiện trigger
-  Bổ sung phản hồi chat tính là đã phản hồi nếu phản hồi ngoài facebook

 #### 2. Fixed
-  Fix lỗi bộ lọc tìm trường động với điều kiện “không có giá trị” bị sai khi phiếu ghi lưu giá trị (-1)
-  Fix lỗi tính năng active khảo sát zalo và facebook
-  Fix lỗi có thể tiếp nhận nhiều hơn số lượng max (phạm vi ảnh hưởng: transfer agent, transfer group, chat vào)
-  Zalo fixbug không view được tin nhắn button trong màn hình agent
-  Fix lỗi xóa DueDate trên ticket không xóa được


### 04-11-2021

 #### 1. Added
-  Bổ sung cho phép gửi email, sms khi ticket không có assignee, hiển thị ở ticket là Unkown
-  Nâng cấp cho phép gửi tin nhắn Zalo ZNS từ bộ lọc người dùng. Hành động chiến dịch. Tin nhắn tự động
-  Bổ sung cơ chế tính SLA V2 mới trong danh sách ticket filter
-  Nâng cấp thay đôi API lấy thông tin hóa đơn gần nhất
-  Chat bot nâng cấp (Tân)
    - Chat bot bổ sung kênh Zalo
    - Điều chỉnh UI/UX giao diện kết nối các kênh Facebook/LiveChat/Zalo sang dạng tabbox. 
    - Menu cố định điều chỉnh cấu hình độc lập theo từng kênh. Facebook giữ nguyên max 3 menu. Zalo + Livechat limit 20 menu.
    - Thêm cấu hình kịch bản ngoài giờ chung

 #### 2. Fixed
-  Fix lỗi load file âm thanh khi nghe online bị lệch ticket
-  Fix lỗi nhập mẫu trả lời phiếu ghi bị reset giá trị trường con
## Tháng 10-2021

### 27-10-2021

 #### 1. Added
-  Ticket filter  bổ sung trường động người dùng ra con mắt nhìn trên danh sách phiếu ghi
-  Click to call bổ sung event post message back thông tin id cuộc gọi khi kết thúc cuộc gọi
-  Nâng cấp thay đôi API lấy thông tin hóa đơn gần nhất
-  Nâng cấp cấu hình hiển thị các trường thông tin users ở giao diện profile khách hàng (khung chat,comment...) đồng bộ với cách xem trong phần user/ticket.
-  Bổ sung filter theo khung giờ báo cáo Thoại - IVR - chi tiết

 #### 2. Fixed
 
### 20-10-2021

 #### 1. Added
-  Bổ sung quyền quản lý token voice API
-  Điều chỉnh hiển thị tên người gửi cho các tin nhắn đồng bộ thành tên Page, OA thay vì lấy tên admin domain

 #### 2. Fixed
-  Fix lỗi cấu hình ticket form không lưu được do service quá dài
-  Fix lỗi cuộn ngang khi ở màn hình điện thoại
-  Fix bug chatbot gán sai người xử lý cuối cùng cho admin
-  Fix bug không hiển thị lại đoạn chat trong lịch sử khi merge người dùng trong lúc đang chat
-  Tối ưu câu lệnh ở ticket filter dành cho QA

### 08-10-2021

 #### 1. Added
-  Nâng cấp button import file trên giao diện livechat mobile
-  Bổ sung chỉ số báo cáo thời gian lấy được SĐT tính thời điểm khách hàng tương tác vào hệ thống lần đầu tiên
-  Bổ sung hàm gửi inbox bình luận v2 (Bổ sung giao diện gửi bình luận)
-  Bổ sung lưu thời điểm lấy sđt, email từ các nguồn Livechat, Facebook, Zalo, Web, Import, API vào User
-  Bổ sung Trigger Survey Facebook

 #### 2. Fixed
-  Fix lỗi Không mở được ticket do thu hồi QA
-  Fix lỗi hiển thị avatar khách hàng với những khách hàng không có avatar
-  Fix lỗi bảo mật update user chéo account id, vượt quyền role
-  Fix bug không update tag ad_id với cácc chat  đồng bộ

## Tháng 09-2021

### 29-09-2021

 #### 1. Added
-  Bổ sung thêm API để lưu nguồn chi tiết
-  Bổ sung Chatbot tự động đóng chat và check trạng thái phản hồi
-  Bổ sung thêm API gửi SMS
-  Bổ sung tạo va gán lead source cho ticket từ API
-  Thay đổi cơ chế báo cáo chuyên viên: lưu trữ dữ liệu on/off của chuyên viên theo ngày
 #### 2. Fixed
-  Fix lỗi tạo trigger điều kiện có giá trị/không có giá trị chuyên viên bị lỗi
-  Fix bug không update avatar zalo user sau khi follow
### 22-09-2021

 #### 1. Added

-  Bổ sung tính năng hiển thị cột thông tin user tương tự ticket (cho phép cấu hình cột thông tin, trường động ra màn hình list contact)
-  Nâng cấp phần ticket form: cho phép sửa được tên nút xác nhận gửi form và setup được giá trị mặc định của trường thuộc tính
-  Bổ sung tính năng gửi hình ảnh cho tin nhắn gửi từ bình luận
-  Bổ sung cơ chế gửi sms chiến dịch cho đúng số điện thoại ở file import

 #### 2. Fixed

### 16-09-2021

 #### 1. Added
-  Bổ sung cảnh báo mất kích hoạt email khi dịch vụ email bị sai thông tin xác thực hoặc mất quyền gmail
-  Bổ sung cấu hình account_config: SEND_NOTIFICATION_TD (cấu hình bật tắt gửi email thông báo phiếu ghi chia việc. default 0. (0: no, 1:yes)). Bật tắt gửi email thông báo cho agent khi có phiếu chia việc được chia cho agent
-  Bổ sung quyền tìm kiếm nhanh chính xác và quyền không hiển thị tìm kiếm chi tiết ở tìm kiếm nhanh
-  Nâng cấp thoại webRTC

 #### 2. Fixed
-  Tối ưu query bình luận facbook, tin nhắn offline, báo cáo ticket, báo cáo tổng quan user
-  Fix lỗi báo cáo inbox zalo, chỉ số đã lấy được sđt đếm sai so với bảng chi tiết
-  Fix lỗi language báo cáo zalo, bổ sung các label còn thiếu

### 08-09-2021

 #### 1. Added
-  Bổ sung tùy chọn “Ưu tiên giao cho nhân viên xử lý” khi tạo chiến dịch tự động
-  Nâng cấp cho phép tìm kiếm theo trường tên nguồn chi tiết và lọc theo nhóm nguồn, nguồn

 #### 2. Fixed
-  Fix lỗi không chọn được điều kiện nhân viên QA trong báo cáo QA
-  Fix lỗi xuất báo cáo QA không ra dữ liệu

### 01-09-2021

 #### 1. Added
-  Bổ sung quyền Xóa ảnh, Chuyển thư mục trong thư viện chung
-  Bổ sung báo cáo "Bình luận facebook": chỉ số số lượng bình luận bị ẩn
-  Bổ sung báo cáo "Phiếu ghi theo thuộc tính khác": điều kiện lọc theo nhóm nguồn

 #### 2. Fixed
-  Fix lỗi hiển thị giao diện IVR (Phím nghe lại)
-  Fix lỗi khảo sát: test lại link khảo sát trên các trình duyệt khác nhau

## Tháng 08-2021

### 25-08-2021

 #### 1. Added
-  Bổ sung Apps ERP trên giao diện ticket và User
-  Bổ sung đánh dấu bỏ qua phiếu ghi bình luận Facebook nếu thực hiện solved hoặc closed qua giao diện ticket mà không có reply
-  Thay đổi tính năng share ticket. Bổ sung share hết thông tin người yêu cầu sang domain được share ticket
-  Chatbot bổ sung lọc bình luận theo điều kiện có sđt, email
-  Cập nhật tính năng các bình luận được đồng bộ lại cũng sẽ tự ẩn theo cấu hình
 #### 2. Fixed
-  Fix lỗi báo cáo tin nhắn chưa xử lý trong báo cáo inbox của báo cáo facebook ghi nhận thiếu trạng thái hoàn thành (closed)
-  Fix lỗi tính năng đếm tin nhắn chưa xem: Trường hợp đang chat với KH có tin nhắn mới KH đó thì vẫn tính chưa đọc và Trường hợp không đọc bấm đã xong luôn vẫn hiển thị thông báo tin chưa đọc
-  Fix bug không xem được đoạn chat có tin survey khi nội dung quá dài

### 20-08-2021

 #### 1. Added
-  Bổ sung báo cáo theo templates
-  Bổ sung Rút gọn link khảo sát qua SMS
-  Bổ sung thư mục và tên ảnh cho kho ảnh chung
-  Bổ sung phân quyền import ảnh nội bộ theo vai trò
-  Chatbot thêm option tự động ẩn bình luận
-  Lưu thông tin thời điểm lấy được sđt, email
-  Bổ sung bỏ qua xử lý chat, comment
-  Bổ sung zalo survey và giao diện cấu hình zalo survey
-  Báo cáo bình luận trong báo cáo Facebook bổ sung chỉ số "Chưa xử lý", "Tỷ lệ chưa xử lý" , "Bỏ qua", "Ẩn", "Đã inbox", "Có số điện thoại", "Tỷ lệ có số điện thoại" ở cả chart và table và table detail
-  Báo cáo Inbox trong báo cáo Facebook bổ sung chỉ số  "Chưa xử lý", "Tỷ lệ chưa xử lý", "Bỏ qua", "Có số điện thoại", "Tỷ lệ có số điện thoại" ở cả chart, table và table detail
-  Báo cáo Inbox trong báo cáo Zalo bổ sung chỉ số "Có số điện thoại", "Tỷ lệ có số điện thoại" ở cả chart, table và table detail
-  Gom các báo cáo thời gian thành 1 menu to "Báo cáo thời gian", bên trong có "Thời gian phản hồi" và "Thời gian giải quyết" trong báo cáo Livechat, Facebook, Zalo
-  Báo cáo QA bổ sung filter đầy đủ như các báo cáo phiếu ghi khác và bổ sung filter theo QA Lead, QA
-  Bộ quy tắc doanh nghiệp bổ sung action Share ticket (chia sẻ lead giữa các domain)
-  Nâng cấp tất cả báo cáo SMS, Email: bổ sung thêm filter theo chiến dịch, có option không thuộc chiến dịch nào

 #### 2. Fixed

### 11-08-2021

 #### 1. Added
-  Bổ sung cấu hình stage kết thúc trong cấu hình trường động ticket kiểu stage
-  Bổ sung cấu hình filter ticket theo kiểu stage
-  Update Api: chỉ cho phép update  End_User, gửi sai role sẽ báo lỗi
 #### 2. Fixed
-  Fix lỗi sai câu lệnh đếm bình luận khi giải quyết bình luận

### 04-08-2021

 #### 1. Added
-  Bổ sung tình trạng gọi chat vào điều kiện cấu hình thỏa thuận dịch vụ
-  Bổ sung webhook event cho trường hợp gọi vào 

 #### 2. Fixed
-  Fix lỗi hiển thị Chọn trường cha/con trong kịch bản gọi ra bị dính 2 chữ “Chọn, chọn”
-  Fix bug Facebook bị tạo ticket survey do xử lý bất đồng bộ
-  Fix bug chatbot vẫn chuyển agent khi khách gửi 2 ảnh
-  Fix bug không chat ra được từ phiếu khi của khách bị merge

## Tháng 07-2021

### 29-07-2021

 #### 1. Added
-  Nâng cấp báo cáo IVR - Thêm điều kiện lọc chuyển ACD
-  Bổ sung Apps View thông tin khách hàng từ ERP
-  Bổ sung App Views thông tin khách hàng từ MISA- MSHOP
-  Nâng cấp chọn giá trị trường động trong tổ chức có tìm kiếm
-  Bổ sung phần bộ lọc trong báo cáo tổng quan và bộ lọc nhóm nguồn trong báo cáo ticket

 #### 2. Fixed
-  Fix lỗi bị mất thông tin trường thuộc tính khi cập nhật phiếu ghi
-  Fix lỗi hiển thị label báo cáo khi chọn lọc k giống nguồn chọn - báo cáo khách hàng tương tác theo kênh

### 21-07-2021
 
 #### 1. Added
-  Tích hợp view App Sakura.vn sang CareSoft
-  Nâng cấp thu hồi ticket đánh giá QA
-  Báo cáo IVR - chi tiết bổ sung trường chi tiết DTMF, Thêm điều kiện lọc chuyển ACD
-  Bổ sung Livechat thay đổi hành vi khi bấm vào ô stackbar để hiện thông tin user (user mở widget trên điện thoại)
-  Bổ sung lưu thông tin phím bấm vào trường động ticket, phục vụ tra cứu và tích hợp theo thông tin
-  Bổ sung Survey facebook ghép tin nhắn survey vào luôn ticket cũ
-  Bổ sung option không lấy bài viết mà khách tag fanpage
-  Bổ sung bảng giám sát tùy chọn theo từng dịch vụ (Tối đa 3 bảng)

 #### 2. Fixed
-  Fix lỗi Lỗi hiển thị trong kịch bản đánh giá (hiển lặp chữ “chọn, chọn” ở loại nội dung chọn 1
-  Fix lỗi label báo cáo tổng quan -> phiếu ghi được tạo theo nhóm nguồn
-  Fix bug đồng bộ định kỳ fb không lấy được ảnh gif 

### 15-07-2021
 
 #### 1. Added
-  Bổ sung giao diện cấu hình gom nhóm Lead Source
-  Bổ sung hiển thị chi tiết thời gian trạng thái trong giám sát từ số phút sang giờ:phút:giây
-  Bổ sung hành động thêm/bớt ccs/follows trong Bộ kích hoạt, quy tắc tự động hóa
-  Bổ sung báo cáo chi tiết cuộc gọi IVR trong báo cáo IVR
-  Bổ sung trả về id tổ chức khi validate trùng tên
-  Bổ sung tính năng: module CommerceWebhook. Đồng bộ ticket từ đơn hàng và tạo contact  khi có đơn hàng trên các kênh bán hàng khác. Option support đẩy data sang hệ thống auto call SmartDialer
-  Chatbot bổ sung option chỉ auto reply với các bình luận gốc và bỏ qua các bình luận con
-  Bổ sung báo cáo theo nhóm nguồn phiếu ghi
-  Bổ sung phân quyền báo cáo IVR theo vai trò

 #### 2. Fixed
-  Fix lỗi không ghi nhận chiến dịch chọn check trùng khi tích chọn trực tiếp từ danh sách
-  Fix lỗi thay thế biến rating_selection trong khi gửi tin nhắn SMS từ trigger và automation
-  Fix lỗi gửi survey qua trigger gửi SMS và EMAIL: phân biệt đúng nguồn vào ticket comment
-  Fix lỗi mẫu trả lời không ghi nhận thuộc tính kiểu nhập văn bản
-  Thay đổi hiển thị báo cáo ngay cả khi user chưa cấu hình trường động dạng stage log nào trong báo cáo Funel ở báo cáo tổng quan

### 07-07-2021
 
 #### 1. Added
-  Bổ sung phân loại trường thuộc tính con (chọn nhiều phương án) với các giá trị giống nhau
-  Bổ sung màn hình cấu hình phiếu ghi. → Nguồn phiếu ghi. [Chi tiết](https://kb.caresoft.vn/support/hc#/?type=article&id=4178)
-  Bổ sung giao diện nguồn ticket phía trên ngày tạo và cập nhật phiếu ghi
-  Bổ sung phần trường động ticket cho phép cấu hình thứ tự hiển thị các lựa chọn cho kiểu dữ liệu chọn 1 hoặc chọn nhiều và cho phép tạo trường con cho trường kiểu stage
-  Bổ sung danh sách ticket cho phép lựa chọn hiển thị tới 300 bản ghi/trang trong danh sách phiếu ghi
-  Nâng cấp báo cáo IVR cho phép hiển thị số lượng cuộc gọi vào đầu số IVR
-  Gộp 3 báo cáo phiếu ghi "Thời gian giải quyết", "Thời gian phản hồi lần đầu trung bình" và "Tỷ lệ phản hồi lần đầu" vào 1 mục với tiêu đề "Các báo cáo theo thời gian"
-  Lưu dữ liệu nguồn ticket
-  Bổ sung tạo lead và nguồn ticket từ các kênh Livechat, Facebook(Inbox và Comment), Zalo, SMS, Lead ads Facebook
-  Bổ sung thời gian phản hồi lần đầu ở kênh Chat theo thời gian Agent trả lời chat

 #### 2. Fixed
-  Fix  lỗi Danh sách bài viết facebook đếm số người được gán khi chuyển agent -> end-user thì vẫn đếm số người được gán (Chat→ Bình luận facebook)
-  Fix lỗi không kích hoạt được thoại trên trình duyệt mobile, màn hình giám sát không hiển thị dữ liệu
-  Fix lỗi phần lọc theo thuộc tính trong báo cáo chiến dịch
-  Fix Lỗi không tự gán kỹ năng cho chuyên viên khi tạo chuyên viên gán vào bộ phận đã có kỹ năng gán sẵn

### 01-07-2021
 
 #### 1. Added
-  Bổ sung trạng thái "**_Outbound_**" trên thanh trạng thái Agent, dành cho những trường hợp Agent thực hiện gọi ra cho khách hàng nhưng không muốn nhận cuộc gọi vào. [Chi tiết](https://gitlab.com/caresoftpublic/documents/-/blob/master/pictures/Trang_thai_Outbound.png)
-  Bổ sung hiển thị báo cáo On/off chuyên viên theo trạng thái thoại (Available, Not Available, Lunch, Typing, Meeting, Training, Outbound). [Chi tiết](https://gitlab.com/caresoftpublic/documents/-/blob/master/pictures/bao_cao_trang_thai_chuyen_vien_moi.png)
-  Bổ sung dữ liệu cho báo cáo update trường first_reply trong ticket_comment khi agent tiếp nhận cuộc gọi vào:lấy luôn comment lúc tạo ticket cuộc gọi nhưng sửa lại created_at của comment thành thời điểm tiếp nhận cuộc gọi thay vì lưu bằng thời gian tạo ticket
-  Báo cáo chiến dịch chi tiết thay đổi filter chọn 1 chiến dịch thành chọn nhiều. [Chi tiết](https://gitlab.com/caresoftpublic/documents/-/blob/master/pictures/chon_nhieu_chien_dich.png)
-  Báo cáo chiến dịch bổ sung phân quyền tìm tất cả, theo bộ phận, cá nhân
-  Chat Widget bổ sung thêm các tracking event gửi ra ngoài iframe: khi có tin nhắn đến, khi box chat đang mở, khi box chat bị đóng, khi widget load xong
-  Bổ sung chat sđt, email tự merge 2 người dùng nếu đã tồn tại ở 1 người dùng khác (accept nếu cả 2 là enduser)
-  Bổ sung module webhook (Hiện support 2 sự kiện cuộc gọi vào/ cuộc gọi ra). Link docs cho webhook event: https://docs.google.com/document/d/1eajDCcbR0U1mXJemWgmwAu_AsKlef17QYURKpiBdZBM/edit
-  Bổ sung tạo ticket nguồn IVR, ticket nguồn IVR hoặc Voice hoặc Voice Out sẽ hiển thị thêm thông tin call_info với tham số start_time, end_time, called, caller

 #### 2. Fixed
-  Fix lỗi giám sát agent tại giao diện CSchat không load lại thông tin định kỳ
-  Fix lỗi lặp tin nhắn lịch sử chat ở 3 kênh Livechat, Facebook, Zalo
-  Fix lỗi api: khi tạo hoặc update contact gửi email hoặc sdt trùng nhau thì không thực hiện. Hiện tại gửi phone_no và phone_no2 giống nhau sẽ insert cả

## Tháng 06-2021

### 23-06-2021
 
 #### 1. Added
-  Bổ sung thông tin Agent ngắt/Khách ngắt trong nội dung comment cuộc gọi vào
-  Báo cáo chiến dịch bổ sung chi tiết phiếu ghi khi click vào chỉ số, cho phép lọc theo chiến dịch,kịch bản, lọc theo thời điểm tạo ticket, xuất dữ liệu
-  Chatbot kịch bản chuyển hướng bổ sung điều kiện thời gian. Hỗ trợ thêm thẻ tạo ticket (thông thường/offline), cho phép chatbot tự tạo phiếu ghi chat offline ngoài giờ với các kênh Facebook, Livechat
-  Chat bổ sung cấu hình tuỳ chỉnh timeout agent đăng xuất khi mất kết nối theo domain
-  Bổ sung Invoice Description cho app KiotViet
-  Thêm filter các bài post facebook đã gán người dùng trong  Chat-->Facebook quản lý bài viết
-  Nâng cơ thay đổi cơ chế popup cửa sổ livechat trên mobile

#### 2. Fixed
-  Fix lỗi không lấy được email về hệ thống khi email có độ dài trường To quá 500 ký tự
-  Fix bug cơ chế phản hồi phiếu ghi bình luận Facebook không vào bình luận cuối cùng trong 1 luồng bình luận cha con mà vào bình luận gốc. Bao gồm cả lỗi Chat Bot tự động inbox lấy tham số tên người dùng là người yêu cầu phiếu ghi thay vì tên của các người dùng Facebook khác cùng comment dưới bình luận

### 18-06-2021
 
 #### 1. Added
-  App Mobile:
    - Bổ sung tính năng number masking (ẩn số điện thoại)
    - Bổ sung thay đổi trạng thái phiếu ghi, chat

#### 2. Fixed
-  Fix lỗi thoại trên Mobile App
-  Fix lỗi tìm kiếm trên Mobile App

### 16-06-2021
 
 #### 1. Added
-  Bổ sung chi tiết thời gian phản hồi lần đầu trong báo cáo thời gian phản hồi. [Chi tiết](https://gitlab.com/caresoftpublic/documents/-/blob/master/pictures/Chi_ti%E1%BA%BFt_th%E1%BB%9Di_gian_ph%E1%BA%A3n_h%E1%BB%93i_l%E1%BA%A7n_%C4%91%E1%BA%A7u.png)
-  Báo cáo Facebook bổ sung nội dung bài post thay vì hiển thị link. [Chi tiết](https://gitlab.com/caresoftpublic/documents/-/blob/master/pictures/b%C3%A0i_%C4%91%C4%83ng_facebook.png)
-  Báo cáo Facebook bảng chi tiết bổ sung click vào thông tin cá nhân sẽ mở contact của CareSoft thay vì link sang Facebook. [Chi tiết](https://gitlab.com/caresoftpublic/documents/-/blob/master/pictures/th%C3%B4ng_tin_c%C3%A1_nh%C3%A2n_1.png)
-  Báo cáo Facebook bảng chi tiết bổ sung sắp xếp theo các trường trong bảng chi tiết
-  Báo cáo lịch sử LiveChat bổ sung filter trạng thái, loại tương tác bổ sung lựa chọn “Tất cả”. [Chi tiết](https://gitlab.com/caresoftpublic/documents/-/blob/master/pictures/Filter_tr%E1%BA%A1ng_th%C3%A1i__lo%E1%BA%A1i_t%C6%B0%C6%A1ng_t%C3%A1c.png)
-  Báo cáo LiveChat bảng chi tiết bổ sung sắp xếp theo các trường trong bảng chi tiết
-  Báo cáo Zalo bảng chi tiết bổ sung tên profile zalo, tên zalo oa kèm đường link đến oa trên zalo. [Chi tiết](https://gitlab.com/caresoftpublic/documents/-/blob/master/pictures/profie-zalo_oa.png)
-  Báo cáo Zalo bảng chi tiết bổ sung profile cá nhân click vào sẽ mở contact của caresoft thay vì link sang zalo. [Chi tiết](https://gitlab.com/caresoftpublic/documents/-/blob/master/pictures/Profile_Zalo.png)
-  Báo cáo lịch sử Zalo bổ sung tìm kiếm theo loại chat, tên profile và tên page. [Chi tiết](https://gitlab.com/caresoftpublic/documents/-/blob/master/pictures/l%E1%BB%8Bch_s%E1%BB%AD_zalo.png)
-  Báo cáo Zalo bảng chi tiết bổ sung sắp xếp theo các trường trong bảng chi tiết
-  Bổ sung phân phối chat theo danh sách agent được gán trong bài post quảng cáo. [Chi tiết](https://gitlab.com/caresoftpublic/documents/-/blob/master/pictures/ph%C3%A2n_ph%E1%BB%91i_chat_theo_b%C3%A0i_post.png)
-  Bổ sung API tickets chi tiết, phần comment thêm addition_detail hiển thị nội dung thông tin kênh giao tiếp comment (ví dụ từ kênh thoại nào hoặc page facebook nào)
-  Bổ sung role_id vào api search users (api/search)
-  Tích hợp App hiển thị đơn hàng trên Mshopkeeper

#### 2. Fixed
-  Báo cáo facebook bảng chi tiết fix lỗi lọc theo trạng thái, loại tương tác hiển thị dữ liệu không đúng
-  Báo cáo facebook bảng chi tiết fix lỗi tìm chi tiết bấm phân trang sau đó tìm kết quả theo tiêu chí khác bị trắng
-  Báo cáo LiveChat fix lỗi chọn trạng thái, loại tương tác hiển thị không đúng dữ liệu
-  Báo cáo LiveChat fix lỗi tìm chi tiết bấm phân trang sau đó tìm kết quả theo tiêu chí khác bị trắng
-  Báo cáo Zalo fix lỗi tìm chi tiết bấm phân trang sau đó tìm kết quả theo tiêu chí khác bị trắng
-  Báo cáo lịch sử Zalo fix lỗi lọc theo trạng thái, loại tương tác hiển thị dữ liệu không đúng
-  Fix bug lỗi phân phối khi agent đổi email đăng nhập lại
-  Fix lỗi tìm contact theo segment ticket mà segmentTicket bị rỗng kết quả → trả lỗi sai

### 09-06-2021
 
 #### 1. Added
-  Nâng cấp báo cáo gọi ra từng đầu số trong định tuyến 3 nhà mạng (Bổ sung filter theo nhà mạng). [Chi tiết](https://prnt.sc/151mg2i)
-  Bổ sung filter dịch vụ gọi ra trong báo cáo lịch sử cuộc gọi (khi chọn loại cuộc gọi ra hoặc chọn tất cả). [Chi tiết](https://prnt.sc/151n1ye)
-  Bổ sung thêm tính năng lọc chiến dịch check trùng trong khi tạo hành động chiến dịch. [Chi tiết](https://prnt.sc/151ovxj)
-  Bổ sung cho phép thay thế nội dung trường động khi tạo hành động gửi SMS và Email

#### 2. Fixed
-  Fix validate cột email khi import khách hàng vào chiến dịch
-  Fix lỗi gửi ticket form in-app
-  Điều chỉnh Zalo Chat chỉ sử dụng API phản hồi và không retry lại bằng API gửi tin chủ động để đảm bảo không mất phí trong màn hình chat Zalo. Tin nhắn gửi ra chủ động có tính phí sẽ được sử dụng ở chiến dịch có cảnh báo

### 02-06-2021
 
 #### 1. Added
-  Nâng cấp API lấy danh sách ticket, cho phép trả về nhiều trường dữ liệu hơn, tương đương chi tiết ticket (ngoại trừ comment)
-  Nâng cấp giao diện cấu hình tin nhắn tự động, cho phép nhìn thấy thời điểm chạy gần nhất và thời điểm chạy tiếp theo. [Chi tiết](https://prnt.sc/15249fw)
-  Bổ sung báo cáo ticket theo dịch vụ
-  Báo cáo thoại bổ sung nhóm theo dịch vụ phần bảng thông số
-  Bổ sung cấu hình account_config SEND_OUT_EMAIL_SOLVED_TICKET: cấu hình tắt bật send email out solved phiếu luôn
-  Bổ sung tính năng Import Ticket từ Excel

#### 2. Fixed
-  Fix lỗi quyền trạng thái chung chuyển sang trạng thái khác available -> mất mạng kết nối lại lại chuyển về trạng thái available

## Tháng 05-2021

### 26-05-2021
 
 #### 1. Added
-  Bổ sung language kênh trạng thái chung Báo cáo Chuyên viên - Lịch sử chuyên viên
-  Bổ sung trường organization_id cho api tạo mới / cập nhật contact
-  Bổ sung thông báo có ticket đổ về

#### 2. Fixed
-  Fix lỗi tiềm ẩn các chức năng: import export người dùng/ticket, gửi email ra, tìm kiếm các phiếu ghi sau khi tạo, phân phối phiếu ghi theo dịch vụ, theo nhóm
-  Fix lỗi automation: điều kiện nhóm chuyên viên lỗi.Thêm log phần trigger
-  Fix lỗi tin nhắn đã phản hồi gửi tin nhắn ra không chat câu nào thì bị tính là chưa phản hồi
-  Fix lỗi giao diện tin nhắn chat bot dạng quick replies (livechat)

### 19-05-2021
 
 #### 1. Added
-  Lịch sử chat bổ sung lọc theo thời gian phản hồi sau 1 thời gian được nhập từ giao diện (áp dụng với 2 filter đã phản hồi và chưa phản hồi)
-  Bổ sung hàm chuyển url ảnh giúp tiền xử lí ảnh phòng trường hợp bị google chặn như lần trước
-  Tối ưu phân phối phiếu ghi
-  Bổ sung trao đổi nội bộ, transfer tới agent có chế độ nghe gọi là divert di động
-  Bổ sung API thêm, cập nhật Tổ chức cho phép update nhiều trường thông tin.[Chi tiết](https://prnt.sc/13duywb)

#### 2. Fixed
-  Fix bug lọc chat chưa phản hồi view những hội thoại gửi ra từ inbox bình luận facebook
-  Fix bug báo cáo thời gian phản hồi bị âm do ghi nhân thời điểm trả lời tin nhắn của các tin trả lời tự động facebook (fix bỏ qua ghi nhận thời gian cho tin đồng bộ từ admin)
-  Fix lỗi báo cáo lịch sử cuộc gọi xuất báo cáo bị lệch cột khi xuất báo cáo role agent hoặc quyền view cá nhân
-  Fix Lỗi tính năng chia việc check trạng thái đăng nhập chuyên viên
-  Fix lỗi email không đồng bộ đúng luồng ticket
-  Sửa giao diện thoại khi mute hoặc hold thì sẽ không thể chuyển nhánh
-  Fix lỗi ticket form nhập vào các trường nội dung ticket hoặc các trường dạng văn bản có ký tự xuống dòng thì trong ticket không hiển thị được theo
-  Chat Fix bug Agent active chat và đóng chat luôn mà không chat gì sẽ bị tính là chat chưa phản hồi

 ### 05-05-2021
 
 #### 1. Added
 -   Nâng cấp bổ sung trường động vào cấu hình thỏa thuận mức dịch vụ (SLA): https://kb.caresoft.vn/support/hc#/?type=article&id=3706

-   Bổ sung trường organization_id vào API chi tiết contact (Api Get /contacts/{id}: [Link](https://drive.google.com/file/d/1JOju5fztylEqKFgKGqNizhm1wvFoo1Gc/view)

-   Đổi hàm Update Agent khi tạo mới Agent

-   Bổ sung thêm cơ chế check hết hạn token zalo realtime. Thêm Thời hạn hết hạn token OA Zalo

-   Bổ sung báo cáo phiếu ghi theo thuộc tính động: Con số tổng (dưới title của biểu đồ) đang hiển thị tổng phiếu ghi => sửa lại thành tổng phiếu ghi có thuộc tính đã lọc ở trên bộ lọc (giống con số tổng ở line tổng của biểu đồ): https://kb.caresoft.vn/support/hc#/?type=article&id=4104

-   Bổ sung filter “_**Đã phản hồi**_” trong lịch sử chat (Livechat, Facebook, Zalo): https://kb.caresoft.vn/support/hc#/?type=article&id=4103

#### 2. Fixed
-   Fix lỗi Redis: Bỏ chế độ cache Redis khi load ra danh sách filter Ticket (getCountTicketAllFilters)

-   Fix lỗi: Ẩn các email đã hủy kích hoạt khi hiển thị ở các màn hình “ Thêm mới hành động chiến dịch, Tạo chiến dịch tự động, Tạo chiến dịch manual

-   Facebook fix lỗi sai thứ tự của tin nhắn tự động phản hồi từ quảng cáo

# Before 05-2021

### 1. Added
-  Trigger - Tự động mời chat theo điều kiện thiết lập: https://kb.caresoft.vn/support/hc#/?type=article&id=1750 
-  Tính năng chia việc: https://kb.caresoft.vn/support/hc#/?type=article&id=1842 
-  Gửi Email khảo sát đánh giá đồng loạt cho 1 tập khách hàng: https://kb.caresoft.vn/support/hc#/?type=article&id=2706 
-  Survey - Khảo sát đánh giá qua Inbox Facebook: https://kb.caresoft.vn/support/hc#/?type=article&id=2003 
-  Merge ticket: https://kb.caresoft.vn/support/hc#/?type=article&id=3530 
-  Copy Ticket - Nhân bản phiếu ghi: https://kb.caresoft.vn/support/hc#/?type=article&id=3487 
-  Tích hợp hệ thống Dialer bổ sung tích hợp CallBot: https://kb.caresoft.vn/support/hc#/?type=article&id=3493
-  Update DEMO Sale Force Automation
-  Nâng cấp hệ thống Dialer bổ sung nhà cung cấp Text To Speech
    - Bổ sung vendor Google Cloud: https://photos.app.goo.gl/NdJQFUKpuZfNxqaZ6
    - Cho phép chọn ngôn ngữ TTS theo tone giọng và các ngôn ngữ khác nhau (Bộ ngôn ngữ của Google): https://photos.app.goo.gl/pq7fHJKv5kW2A7sy9
    - Bổ sung cấu hình cho Vendor Cloud Google: https://photos.app.goo.gl/ZRi29xTFHDnD4aTy7
    - Cho phép phát lặp TTS (1-10 lần): https://photos.app.goo.gl/t9HxYdB78DjiHLeZ8
    - Cho phép giới hạn thời lượng cuộc gọi TTS: https://photos.app.goo.gl/4TxMQ3MXK2bUTspk7
-  Chatbot: https://kb.caresoft.vn/support/hc#/?type=section&id=564 
-  Bộ quy tắc doanh nghiệp: Bộ kích hoạt(Trigger): https://kb.caresoft.vn/support/hc#/?type=article&id=4048 
-  Bộ quy tắc doanh nghiệp: Quy tắc tự động hóa (Automation): https://kb.caresoft.vn/support/hc#/?type=article&id=4049 
-  Bộ quy tắc doanh nghiệp: Thỏa thuận mức dịch vụ: https://kb.caresoft.vn/support/hc#/?type=article&id=3706 
-  Ticket form: https://kb.caresoft.vn/support/hc#/?type=article&id=3629 
-  Ticket Sharing: https://kb.caresoft.vn/support/hc#/?type=article&id=3652 
-  Chatbot nâng cấp bổ sung: 
    - Thêm tuỳ chọn phản hồi bằng comment trong công cụ Auto Inbox. Comment phản hồi tự động sẽ đồng bộ vào Phiếu ghi của khách hàng dưới tên Admin. Tên Bot và tên luật tự động Inbox sẽ được mô tả trong phần mô tả comment (addition details).
    - Thêm tuỳ chọn hỗ trợ chèn Emoji và Danh sách thuộc tính người dùng trong các màn hình soạn thảo: 
        - Nội dung tin nhắn của Thẻ Văn bản 
        - Nội dung tin nhắn phản hồi và nội dung comment phản hồi của Công cụ Auto Inbox
    - Danh sách thuộc tính: Tên, SĐT, Email, Note và các trường động đang active trên domain
    - Thẻ Kết nối Agent bổ sung: Cho phép cấu hình theo Nhóm Agent. (Tự động làm mới lại danh sách agent trong từng nhóm định kỳ 5 phút)
    - Dashboard danh sách Bot bổ sung view thêm các Kênh mà Bot đang kết nối
-  Nâng cấp kho lưu trữ ảnh chung: https://kb.caresoft.vn/support/hc#/?type=article&id=4050
-  Nâng cấp màn hình cấu hình lịch làm việc (chưa chạy mới lên giao diện): https://photos.app.goo.gl/nAP4Dsr42UsZJ1au9
-  Bổ sung giao diện check trùng tạo ticket chiến dịch với các chiến dịch khác: https://photos.app.goo.gl/5cUAX5yymNj61EQh6
-  Bổ sung cấu hình widget chat trên di động: https://photos.app.goo.gl/kPzNbM6RiHLJ3XxR7
-  Bổ sung ngôn ngữ tiếng Thái
-  Bổ sung chặn quyền chỉnh sửa thông tin khách hàng của Agent: https://kb.caresoft.vn/support/hc#/?type=article&id=2953
-  Bổ sung cho phép chọn được dịch vụ SMS trên giao diện gửi SMS: https://kb.caresoft.vn/support/hc#/?type=article&id=4051
-  Bổ sung cho phép cấu hình nhiều SMS Brandname trên CareSoft: https://kb.caresoft.vn/support/hc#/?type=article&id=2875
-  Bổ sung cho hiển thi nội dung trong mục lich sử comment khi phiếu ghi để chế độ pending: https://kb.caresoft.vn/support/hc#/?type=article&id=4052
-  Bổ sung tìm theo giờ trong segment tổ chức, người dùng: https://kb.caresoft.vn/support/hc#/?type=article&id=1849
-  Bổ sung thêm cột thời gian phản hồi lần đầu trong phần chi tiết của báo cáo phản hồi lần đầu: https://prnt.sc/122kvhy
-  Bổ sung bộ lọc chat, cho phép tìm theo thuộc tính động của khách hàng (tìm theo kiểu chọn/chọn nhiều): https://prnt.sc/122kya2
-  Bổ sung quyền đăng ký nhận webhook event leadgen khi thêm mới Page Facebook
-  Bổ sung kịch bản hỏi chiến dịch cho phần Predict Call - Smart Dialer
-  Bổ sung cấu hình xác nhận trước khi ngắt cuộc gọi: https://kb.caresoft.vn/support/hc#/?type=article&id=4053
-  Tạo thêm bộ lọc nguồn Livechat và FB trong tin nhắn offline: https://kb.caresoft.vn/support/hc#/?type=article&id=4068
-  Update bổ sung tìm kiếm agent theo email hoặc số phone trong API getList Agent (bổ sung param : email_phone)
-  Bổ sung tích hợp 2 hệ thống Caresoft và Zendesk: 
    - Các tham số cấu hình App: API url, api username, api token. Lấy trên giao diện admin Zendesk
    - Gán App zendesk theo dịch vụ. Bảng apps_service
-	Tích hợp Haravan
-	Bổ sung thêm cỡ chữ trong soạn thảo Email: https://kb.caresoft.vn/support/hc#/?type=article&id=4069
-	Bổ sung cấu hình ẩn bình luận theo bài post: https://kb.caresoft.vn/support/hc#/?type=article&id=1777
-	Nâng cấp thêm nguồn tin nhắn trong thông báo khi có tin nhắn:https://kb.caresoft.vn/support/hc#/?type=article&id=4070
-	Bổ sung cấu hình thêm trường vào màn hình Thông tin của Livechat - Lưu thông tin trường động khi khách hàng nhập: https://kb.caresoft.vn/support/hc#/?type=article&id=4071
-	Thêm cảnh báo nhập các trường động bắt buộc nhập (người dùng và phiếu ghi) khi bấm "Đã xong" ở màn hình Chat và Bình luận Facebook (Hiện tại đang chỉ validate người dùng): https://kb.caresoft.vn/support/hc#/?type=article&id=4072
-	Bổ sung tìm kiếm nhanh theo DueDate: https://kb.caresoft.vn/support/hc#/?type=article&id=4073
-	Bổ sung thêm thông tin cho báo cáo phản hồi lần đầu
-	Bổ sung cấu hình “Tắt tính năng gửi email ra cho KH khi phản hồi comment facebook”: https://kb.caresoft.vn/support/hc#/?type=article&id=4074
-	Bổ sung validate phiếu ghi khi đóng comment hoặc chat offline, shortcut cho phép nhập thông tin phiếu ghi nhanh: https://kb.caresoft.vn/support/hc#/?type=article&id=4076
-	Nâng cấp xem video trực tiếp trong phần inbox facebook: https://kb.caresoft.vn/support/hc#/?type=article&id=4077
-	Bổ sung hiển thị nội dung trường tổ chức trong mục danh sách khách hàng: https://kb.caresoft.vn/support/hc#/?type=article&id=4078
-	Bổ sung trả về psid trong Api getContact
-	Nâng cấp IVR cho phép ghi nhận phím bấm và tạo ticket ở nhánh IVR: https://kb.caresoft.vn/support/hc#/?type=article&id=4079
-	Lưu dữ liệu báo cáo cuộc gọi IVR: https://kb.caresoft.vn/support/hc#/?type=article&id=4080
-	Nâng cấp API smartdialer: https://kb.caresoft.vn/support/hc#/?type=article&id=2029 
    -	Api danh sách Agents rảnh - Trả về chuỗi Json danh sách user kèm trạng thái có thể tiếp nhận được cuộc gọi hay không
    -	Api kiểm tra các cuộc gọi đang lên lịch để gọi ra(Sẵn sàng để gọi hoặc đang chờ để vào Queue) - Trả về danh sách dữ liệu số điện thoại sẽ gọi và trạng thái lên lich(Đã vào Queue hay đang chờ)
    -	Api xoá cuộc gọi đã lên lịch - Dữ liệu cho phép xoá bỏ 1 cuộc gọi đã lên lich(Cuộc gọi đang chờ vào Queue)
-	Bổ sung Apps Zoho trên caresoft
-	Bổ sung giới hạn tốc độ gửi tin facebook messenger qua chiến dịch (Tạo hành động chiến dịch): https://kb.caresoft.vn/support/hc#/?type=article&id=4081
-	Bổ sung cấu hình ghép phiếu ghi gọi nhỡ bỏ qua phiếu ghi đã ở trạng thái closed hoặc solved
-	Bổ sung cấu hình ghép phiếu ghi chat bỏ qua phiếu ghi đã ở trạng thái closed hoặc solved
-	Bổ sung tính năng follower theo từng bài post Facebook: https://kb.caresoft.vn/support/hc#/?type=article&id=1777
-	Bổ sung cấu hình timeout khi người dùng nhập liệu trên IVR, trước đây mặc định 1,5s, giờ cho phép người dùng tự cấu hình: https://kb.caresoft.vn/support/hc#/?type=article&id=4079
-	Nâng cấp gửi tin nhắn facebook fix lỗi chặn ngoài giờ. Retry gửi lại bằng tin nhắn có gắn thẻ với Page bật cấu hình gắn thẻ cho tin nhắn (Hiện có 4 loại thẻ đang hỗ trợ: 
    -	CONFIRMED_EVENT_UPDATE
    -	POST_PURCHASE_UPDATE
    -	ACCOUNT_UPDATE
    -	HUMAN_AGENT
    -	Test: Gửi text + các loại attachment
    -	Gửi tin nhắn từ chiến dịch cho khách cũ. Mỗi thẻ sẽ ràng buộc về nội dung tin nhắn gửi ra được phép và không được phép khác nhau. Chi tiết documents: https://developers.facebook.com/docs/messenger-platform/send-messages/message-tags)
-	Bổ sung chức năng cấu hình API trong giao diện Admin: https://kb.caresoft.vn/support/hc#/?type=article&id=1769
-	Tạo thêm trên mục tìm kiếm của tin nhắn phần Chưa phản hồi: https://kb.caresoft.vn/support/hc#/?type=article&id=4082
-	Báo cáo tìm kiếm nghe lại cuộc gọi bổ sung bộ phận: https://kb.caresoft.vn/support/hc#/?type=article&id=4083
-	Tìm kiếm nhanh thay đổi cơ chế, Gõ Enterter mới tìm, bỏ chế độ vừa gõ vừa search
-	Bổ sung cấu hình confirm khi đóng chat, bình luận: https://kb.caresoft.vn/support/hc#/?type=article&id=4084
-	Bổ sung tìm kiếm theo ngày giờ chi tiết (bộ lọc cschat): https://kb.caresoft.vn/support/hc#/?type=article&id=4085
-	Bổ sung cột số chat chưa phản hồi trong phần giám sát agent cschat. [Ảnh](https://prnt.sc/12lpbvn)
-	Bổ sung tìm kiếm nhanh theo trường động ticket ngày tháng. [Ảnh](https://prnt.sc/12lpfzf)
-	Bổ sung số chat chưa phản hồi cho giám sát chat agent. [Ảnh](https://prnt.sc/12lpbvn)
-	Lọc tin nhắn đã xem hay chưa xem trong phần chat. [Ảnh](https://prnt.sc/12lpp2b)
-	Thêm cấu hình mở box confirm khi đóng comment
-	Nâng cấp cấu hình trường cha /con kịch bản cuộc gọi, nhân bản kịch bản cuộc gọi, nhân bản ticket, xuống dòng câu hỏi kịch bản gọi.
    - Cấu hình trường cha con kịch bản cuộc gọi: https://kb.caresoft.vn/support/hc#/?type=article&id=2681
    - Nhân bản kịch bản cuộc gọi: https://kb.caresoft.vn/support/hc#/?type=article&id=3488
    - Nhân bản ticket: https://kb.caresoft.vn/support/hc#/?type=article&id=3487
-	Nâng cấp cơ chế phân phối cuộc gọi bắt buộc theo độ ưu tiên: cho phép phân phối cuộc gọi theo thứ tự ưu tiên 1,2,3,4,5
-	Bổ sung tính năng check lỗi câu hỏi cho phần kịch bản gọi ra
-	Bổ sung cấu hình thẻ tin nhắn cho Facebook
-	Bổ sung quyền chặn chat chủ động ra đối với những cuộc chat được đánh dấu là chưa xử lí
-	Bổ sung nhạc hold khi transfer cuộc gọi
-	Bổ sung cấu hình cho phép thêm địa chỉ email bổ sung khi gửi import, export (cấu hình account config ADDITION_EMAIL_ADDRESS_SEND_EXPORT, ADDITION_EMAIL_ADDRESS_SEND_IMPORT)
-	Báo cáo tổng quan - phiếu ghi được tạo theo dịch vụ: Bổ sung thêm prefix vào tên dịch vụ. [Ảnh](https://prnt.sc/12lsew0)
-	Chức năng merge ticket bổ sung thêm option copy comment ticket: https://kb.caresoft.vn/support/hc#/?type=article&id=3530
-	Bổ sung apps CS Embed Apps
-	Chatbot Định tuyến Agent theo danh sách cấu hình: https://kb.caresoft.vn/support/hc#/?type=article&id=4097
-	Chatbot LiveChat không gặp agent tự gán cho admin và tạo phiếu ghi cho admin
-	Chat Bot bổ sung tính năng sao chép bot, sắp xếp ảnh trong slide, đồng bộ menu cố định Facebook
    - Sao chep bot: https://kb.caresoft.vn/support/hc#/?type=article&id=3531
    - Sắp xếp ảnh trong slide: https://kb.caresoft.vn/support/hc#/?type=article&id=4092
    - Đồng bộ menu cố định Facebook: https://kb.caresoft.vn/support/hc#/?type=article&id=3534
-	Chiến dịch gửi tin nhắn Facebook + Gửi tin nhắn thủ công từ bộ lọc + Tin nhắn tự động Facebook bổ sung tạo ticket: [Link](https://kb.caresoft.vn/support/hc#/?type=article&id=2712)
    -	Nguồn: Inbox Facebook.
    -	Dịch vụ: Facebook Page gửi ra
    -	Chiến dịch: Lưu chiến dịch gửi ra nếu gửi từ chiến dịch
    -	Tình trạng gọi/chat: đánh dấu = nhỡ nếu gửi lỗi.
    -	Lưu kết quả thành công hoặc lỗi khi gửi vào ticket comment
-	Bổ sung nhạc khi transfer ra số di động bên ngoài
-	Bổ sung tính năng tùy chỉnh email cho từng agent gửi ra riêng biệt: https://kb.caresoft.vn/support/hc#/?type=article&id=4105
-	Bổ sung sử dụng mẫu trả lời nhanh chat khi nhắn tin riêng cho khách hàng từ comment 
-	Nâng cấp về mẫu trả lời chat, cho phép thiết lập tự động thêm trường động phiếu ghi khi chuyên viên đang Chat và chọn mẫu trả lời Chat: https://kb.caresoft.vn/support/hc#/?type=article&id=2841
-	Bổ sung Kích hoạt thoại bổ sung cơ chế chờ 5s mới có thể nghe gọi. Nếu user sử dụng 2 tab, 1 tab incall mà tab kia kích hoạt thoại thì tab incall sẽ popup confirm có muốn ngắt máy để chuyển quyền nghe gọi sang tab kia không. Bấm có sẽ Đồng ý, bấm Hủy hoặc timeout 5s sẽ Không đồng ý
-	Bổ sung ứng dụng Motosoft
-	Bổ sung cơ chế load trigger khi đổi url trên các trang web dạng single page app
-	Bổ sung ứng dụng GenCRM: 
    -	Xem thông tin khách hàng, thông tin đơn mua
    -	Tạo đơn hàng từ CS và gọi sang link của GENCRM
-	Update trường cho Motosoft (Bỏ email, bỏ ngày nhận. ngày giao)
-	Nâng cấp ticketFilter cho phép chọn tag và show tag của ticket ra danh sách phiếu ghi: https://kb.caresoft.vn/support/hc#/?type=article&id=4106
-	Bổ sung điều kiện thời gian so sánh với giờ hiện tại trong ticketFilter: [Link](https://drive.google.com/file/d/12-_nGDQ1STMqjjKb-ifp5IEQlAskEkQA/view)
-	Nâng cấp ticketFilter bổ sung trường hiển thị tổ chức ra ngoài (Chọn ở phần cấu hình các cột dữ liệu): [Ảnh](https://prnt.sc/12ltzxt)
-	Nâng cấp Kiot việt bổ sung các trường (ngày sinh, ghi chú, doanh thu, nhóm khách hàng) ra màn hình apps
-	Bổ sung trường dữ liệu hiển thị trên apps motosoft các trường “Nhân viên lắp đặt, tư vấn sửa chữa, Nội dung sửa chữa, cửa hàng”..
-	Bổ sung app IPOS lên hệ thống
-	bổ sung tính năng chuyển chat trong bộ phận: [Link](https://prnt.sc/12luyjm)
-	Chatbot bổ sung cấu hình Timeout Agent (Cho phép chuyển hướng tới 1 kịch bản khác nếu sau 1 khoảng thời gian không có agent tham gia chat): https://kb.caresoft.vn/support/hc#/?type=article&id=4097
-	Bổ sung Tìm theo kịch bản Automessage trong segment Tickets: [Link](https://prnt.sc/12lv2ir)
-	Facebook quản lý bài post bổ sung các cấu hình ẩn tuỳ chỉnh (theo sđt, email hoặc bỏ qua ẩn): [Link](https://prnt.sc/12lv6ep)
-	bình luận chat offline bổ sung cho phép tìm theo sđt, email, tag người yêu cầu: [Link](https://prnt.sc/12lvh6p)
-	bổ sung filter mặc định chat, bình luận theo kỹ năng mà agent được gán
-	Bổ sung tính năng API Search tìm nhiều đối tượng trên 1 API duy nhất: https://docs.google.com/document/d/1FPFGYjsy9KwX-Dtoq88cXSSXeHj2OegIjbvGEt7d858/edit 
-	Nâng cấp, bổ sung tính năng báo cáo theo bài viết - Bổ sung vào bộ lọc tìm ticket theo ID bài viết facebook: [Link](https://prnt.sc/12lvjms)
-	Facebook chat ra từ phiếu ghi bình luận sẽ tự ghép vào phiếu ghi bình luận. 
o	Các luồng chat ra chủ động hoặc chat vào sẽ ghép vào phiếu ghi tạo gần nhất nguồn Inbox Facebook hoặc nguồn Facebook bình luận đk đã được inbox (nếu bật cấu hình FACEBOOK_MERGE_TICKET_CHAT_WITH_TICKET_COMMENT_REPLIED
-	Chat bổ sung phân quyền chặn từ server cấu hình không cho agent chủ động chat ra với các chat chưa gán (bật chặn active chat từ mobile, DISABLE_REQUEST_ACTIVE_CHAT_UNASSIGNED)
-	Bổ sung App Odoo
-	Nâng cấp thêm param trong tính năng "Liên kết đến hệ thống CRM
-	Bổ sung view apps Netsuite thông tin khách hàng trên CareSoft
-	Tạo phiếu ở trạng thái Open luôn, CREATE_TICKET_STATUS_OPEN
-	Thêm api lấy dữ liệu campaign theo kịch bản: [Link](https://drive.google.com/file/d/1JOju5fztylEqKFgKGqNizhm1wvFoo1Gc/view)
-	Facebook bổ sung cấu hình gắn tag user, ticket chat thông tin website nhúng plugin chat nếu người dùng chat vào Facebook từ website
-	Tích hợp App tuelinhcrm
-	Tự tách view tin nhắn trên Caresoft thành nhiều đoạn khi gửi inbox bình luận > 1000 ký tự
-	Tạo phiếu ghi con sẽ lấy thông tin người yêu cầu của phiếu ghi cha
-	Bổ sung điều kiện filter Ticket, segment tìm có giá trị/ và không có giá trị với dữ liệu dạng lựa chọn
-	Thay đổi cơ chế tìm kiếm theo tag, suggest ra tag cho user chọn sau đó tìm kiếm: [Ảnh](https://prnt.sc/12lwpex)
-	Bổ sung index động cho báo cáo thoại, gọi ra, gọi vào, gọi vào thành công
-	Bổ sung chức năng chọn người yêu cầu là chính mình khi tạo phiếu ghi mới: [Ảnh](https://prnt.sc/12lwsec)
-	Bình luận chưa xử lý hoặc đang xử lý sẽ có màu xanh: [Ảnh](https://prnt.sc/12lww9c)
-	Bổ sung API thêm hành động vào chiến dịch: [Link](https://drive.google.com/file/d/1JOju5fztylEqKFgKGqNizhm1wvFoo1Gc/view)
-	Bổ sung giao diện bình luận đã phản hồi (có thêm icon thể hiện): [Ảnh](https://prnt.sc/12lxiz8)
-	Bổ sung xóa chiến dịch (chỉ chiến dịch đã kết thúc), Xóa kịch bản, xóa kịch bản QA
-	Bổ sung mở/phóng to ảnh trực tiếp khi xem ở cửa sổ xử lý bình luận
-	Bổ sung UX thoai: Khi có cuộc gọi đến, thông tin đầu số dài quá chữ sẽ tự động chạy hết nội dung
-	Bổ sung tên dịch vụ của cuộc gọi vào trong nội dung comment của ticket: [Ảnh](https://prnt.sc/12lxq8o)
-	Bổ sung 4 trạng thái thoại cho agent ( Meeting, Lunch, Training, Typing ) Bên cạnh 2 trạng thái hiện thời là Available và Not Available ( Nút đổi cạnh icon email): https://kb.caresoft.vn/support/hc#/?type=article&id=30
-	Bổ sung vào màn hình Thoại tab Hoạt động hiện tại các thông số mới ( Số cuộc gọi đang chờ dưới 60s, Số cuộc gọi đang chờ trên 60 giây, Tỉ lệ Abandon, Tổng số cuộc gọi đang tiếp nhận, Tổng số agent đang Available, Tổng số Agent nghỉ ăn trưa (lunch) , Tổng số agent Meeting, Tổng số aent đang đợi xử lý xong call trước đó , tổng số agent đang ở trạng thái đào tạo (training): https://kb.caresoft.vn/support/hc#/?type=article&id=3995
-	Sắp xếp lại giao diện giám sát agent và bổ sung các trạng thái mới của agent: https://kb.caresoft.vn/support/hc#/?type=article&id=3995
    -	Thời điểm đăng nhập
    -	Tổng thời gian avaiable
    -	Tổng cuộc gọi vào
    -	Tổng cuộc gọi ra
    -	Thời gian xử lý trung bình
    -	Thời gian ở trạng thái hiện tại
-	Chat bot bổ sung option lưu giá trị vào người dùng qua các thẻ Nhập thông tin, Trả lời nhanh: https://kb.caresoft.vn/support/hc#/?type=article&id=4093
-	Thêm trigger, automation từ điều kiện trường động của ticket và user
    - Trigger: https://kb.caresoft.vn/support/hc#/?type=article&id=4048
    - Automation: https://kb.caresoft.vn/support/hc#/?type=article&id=4049
-	Nâng cấp một số điều kiện của trigger theo trường contact
-	Chatbot bổ sung Thẻ tự động chuyển kịch bản theo thuộc tính người dùng: https://kb.caresoft.vn/support/hc#/?type=article&id=4098
-	Api bổ sung update trường “Nhân viên chỉ định chiến dịch”: [Link](https://drive.google.com/file/d/1JOju5fztylEqKFgKGqNizhm1wvFoo1Gc/view)
-	Thêm tính năng xóa ảnh đã import trên kho ảnh nội bộ:https://kb.caresoft.vn/support/hc#/?type=article&id=4107
-	Bổ sung nguồn ticket Inbox Facebook Out cho chat ra: [Ảnh](https://prnt.sc/12m4hbc)
-	Chatbot thêm loại Webview cho thẻ Văn bản: https://kb.caresoft.vn/support/hc#/?type=article&id=4090
-	Lưu Feedback survey email vào ticketcomment: [Ảnh](https://prnt.sc/12m4sni)
-	Chatbot tin nhắn tự động inbox comment bổ sung hình ảnh và ghi tên Bot vào người gửi
-	Bổ sung App HasonHaiVan
-	Bổ sung cấu hình account_config NUM_CALL_CHANGE_NO_ANSWER_IPPHONE:so luong cuoc goi ko tra loi se bi chuyen thanh no answer(thoại ipphone). default 3. Là số cuộc không trả lời cuộc gọi liên tục sẽ bị chuyển thành no answer với thiết bị nghe gọi ipphone/softphone có ngồi máy
-	Chatbot tự chuyển kịch bản chào hỏi khi bắt đầu chat
-	Hỗ trợ phát triển phần copy thông tin thẻ tag, cho phép bấm vào tag là copy tag
-	Nâng cấp thêm phần xem full báo các cho các vai trò, cho phép mở xem full báo cáo cho các vai trò thấp hơn Admin và Sub-admin
-	Chatbot thêm option chỉ gửi bình luận không gửi inbox từ bình luận: giao diện thay đổi bổ sung cho phép lựa chọn 1 trong 2 option: tự động gửi bình luận hoặc tự động gửi inbox từ bình luận. https://kb.caresoft.vn/support/hc#/?type=article&id=3501
-	Nâng cấp js phần Ticket Filter. Xử lý trùng lặp request
-	Thêm UI loading khi hệ thống đang kết nối tới websocket
-	Facebook thay đổi cơ chế upload ảnh (Giảm thời gian gửi ảnh từ EndUser tới Agent và khôi phục được ảnh khi Facebook lỗi download. Giảm băng thông và Storage Caresoft)
-	Tối ưu cơ chế load conversation fb, zalo, web: Lúc đầu ko load full mà theo timerange 30 ngày
-	Update API Tạo, Cập nhật người dùng thêm trường follower_id, city_id, district_id, user_info1 -> user_info
-	Nâng cấp hiệu năng phần phân phối thoại, chat, phiếu ghi
-	Nâng cấp Facebook xử lý không nhận comment realtime. (Note Dev: Facebook API không query detail được comment tại thời điểm nhận webhook event)
-	Facebook : Đổi cơ chế load post qua db
-	Chỉnh lại cơ chế ghi nhận phản hồi lần đầu
-	Đổi cơ chế láy thông tin bài post mặc định qua db
-	Nâng cấp Gmail API
-	Thêm cơ chế chặn cho Api CS: gọi sai quá 20 lần sẽ cache lại và trả về thông báo luôn, ko xử lý logic gì cả giảm tải server.
-	Bổ sung trường thông trong api phiếu ghi và api log calls.
-	Nâng cấp phiên bản webrtc hỗ trợ cho Chrome version >=81
-	Thêm API lịch sửa chat
-	Chatbot đổi cơ chế bắt keywords từ bằng sang chứa 1 trong các keyword
-	Chatbot Auto Inbox Comment: Nâng cấp giao diện nhập keywords từ text input sang chip input. [Ảnh](https://prnt.sc/12m69id)
-	Chatbot Setup AI: Nâng cấp giao diện nhập keywords từ text input sang chip input. [Ảnh](https://prnt.sc/12m66y0)
-	Nâng cấp phiên bản thoại web
-	Sửa cơ chế gửi inbox từ profile (sfa đã sửa, port sang caresoft)
-	Sửa api lấy lịch sử chat
-	Sửa api tạo tạo và update ticket với trường động: cho phép update về rỗng
-	Facebook fix bug mở chính xác tới hộp thoại chat của khách hàng từ CareSoft
-	Cập nhật hiển thị thêm box inbox khi API ẩn comment bị lỗi
-	Chat bổ sung cơ chế đóng hội thoại chủ động trước khi stop Socket server - Fix lỗi 1 số hội thoại không tạo ticket khi upcode
-	Nâng cấp chat client bổ sung tính năng auto popup với các trường hợp nhúng vào app bên thứ 3, fix lỗi không hiển thị chat khi gọi bằng link chat trực tiếp
-	Chỉnh sửa UX cho phần box thoại: cho phép bấm vào biểu tượng thoại sẽ hiển thị ra ngay bàn phím nhập số để gọi
-	Chỉnh sửa ux phần macro phiếu ghi: cho phép dùng phím điều hướng để chọn Macro phiếu ghi
-	Thay đổi cơ chế tính giây khi gọi ra: khách hàng nhấc máy mới bắt đầu tính giây
-	Facebook login bổ sung đăng ký event messaging_referrals + Chat bổ sung lưu dữ liệu referrals gồm ad_id., post_id nếu có.
-	Thay đổi hiển thị access token và reset access token trong cấu hình api: token giờ sẽ chỉ dùng được khi gọi api lấy dữ liệu , không dùng cho tích hợp voice api
-	Nâng cấp phân hệ ip phone/softphone
-	Zalo chat nâng cấp api phản hồi cho user chat từ website mà không follows OA
-	Chat Facebook Fixbug không gán ID QC +1 case chat không hiển thị tin nhắn gửi link
-	Livechat bổ sung auto tag ticket, user theo utm_source, utm_campaign từ nguồn Google Ads
-	Chatbot bổ sung hỗ trợ xuất JSON Code cho các thẻ Văn bản, Trả lời nhanh, Slide để nhúng vào mã quảng cáo Facebook: https://kb.caresoft.vn/support/hc#/?type=article&id=4102
-	Lịch sử chat bổ sung icon nhận biết tin nhắn nào của agent gửi ra, reformat lại text nội dung tin nhắn (gửi mã code js, html có thể hiển thị ra được nguyên đoạn mà không bị cắt text hay hiển thị thành các ký tự đặc biệt). Tất cả các kênh
-	Bổ sung thêm dữ liệu dịch vụ gọi ra cho khách hàng phiếu ghi. [Ảnh](https://prnt.sc/12m6ldo)
-	Bổ sung phân quyền macro (mẫu trả lời ticket) theo vai trò, bộ phận, user (giống phân quyền trên ticket filter):https://kb.caresoft.vn/support/hc#/?type=article&id=224
-	Bổ sung cảnh báo mất kích hoạt Zalo OA
-	Bổ sung update token khi thêm mới Zalo OA đã tồn tại
-	Bổ sung hỗ trợ preview bài viết video Facebook
-	Bổ sung tự động online thoại khi kết nối ổn định (sau khi mạng chập chờn và có kết nối lại)
-	Bổ sung lưu đường dẫn truy cập vào ticket với Live chat có agent tiếp nhận
-	bổ sung đếm charater trong sms ở Campaign và tạo kịch bản SMS
-	Bổ sung Tình trạng gọi/chat khi xuất dữ liệu phiếu ghi từ bộ lọc
-	Bổ sung tự động cập nhật online thoại khi bị mất kích hoạt
-	Bổ sung cột tag khi xuất phiếu, người dùng từ bộ lọc
-	Bổ sung đánh dấu gửi email / sms thất bại (gửi thường, gửi từ chiến dịch, gửi từ bộ lọc)
-	Bổ sung ID người yêu cầu export Excel bộ lọc phiếu ghi
-	Bổ sung cơ chế tách SĐT từ inbox, comment ràng buộc thêm đầu số (tránh lấy mã số thuế cùng độ dài)
-	Tự động cập nhật số divert khi agent đôi số điện thoại
-	Bổ sung tìm kiếm trong description trên KB
-	Khung chat của Agent: Chứa luôn cả message của Trigger
-	Bổ sung phân quyền theo vai trò cho chức năng ban ip
-	Bổ sung cột “Đáp ứng SLA” và “Tên tổ chức” khi xuất phiếu ghi từ bộ lọc
-	Thêm trường tên chuyên viên trong báo cáo lịch sử chat. [Ảnh](https://prnt.sc/12m7dx6)
-	Báo cáo chiến dịch bổ sung hiển thị loading khi lấy dữ liệu
-	Chặn (Ban) ip khách, khách hàng bị ban ip sẽ không hiển thị live chat trên web nữa, áp dụng trên tất cả domain : https://kb.caresoft.vn/support/hc#/?type=article&id=2770 
-	Update bộ đếm tin sms: đếm phía server theo chuẩn GSM, trước đây đếm theo client, khi gửi nếu bỏ dấu sẽ không đúng nữa
-	Bổ sung thời gian đồng bộ facebook lên max 6 tháng
-	Bổ sung xuất excel biểu đồ
-	Bổ sung trường thông tin cho apps CMS Edu
-	Bổ sung thêm phần đếm hội thoại trong phần lịch sử chat như phần đang chat
-	Update phần ẩn số điện thoại (Ẩn phần thông báo trình duyệt)
-	Bổ sung sắp xếp Article trong màn hình soạn thảo HelpCenter (Sắp xếp bài mới nhất lên đầu) -- Import bài viết,
-	Bổ sung google analytics event handling cho live chat
-	Bổ sung thêm option copy comment bị thiếu trong chức năng mergeticket ở bộ lọc
-	Bổ sung báo lỗi giới hạn ký tự với trường động dạng văn bản khi import người dùng vào hệ thống
-	Bổ sung cột nguồn tạo khách hàng khi xuất dữ liệu thông tin khách hàng từ segment
-	Giảm thời gian thực hiện trigger khi tạo và update ticket xuống 30s (trước đây là 5 phút)
-	Update báo cáo: hiển thị chi tiết các dữ liệu group và chuyên viên rỗng hoặc null
-	bổ sung chế độ gửi tin nhắn từ bình luận nhưng không nhảy sang tab “Đang chat” (vẫn tạo hội thoại)
-	Bổ sung hiển thị apps KiotViet sang màn hình user
-	Bổ sung key cho form gửi dữ liệu sang app của khách trên app CMS EDU
-	bổ sung các hành động trước và sau khi thêm, sửa, xóa ticket form
-	Facebook bổ sung đồng bộ định kỳ Lead Ads
-	Facebook update lấy chat của những user khoá tài khoản nhưng vẫn active Messenger
-	Ticket form bổ sung chức năng in app widget cho phép điền sẵn thông tin username, sđt, email: https://kb.caresoft.vn/support/hc#/?type=article&id=3629
-	Zalo nâng cấp hỗ trợ lấy chat từ website nhúng zalo (Tin nhắn không follow OA)
-	Nâng cấp Apps Motosoft, Bỏ một số trường hiển thị theo Yc của khách hàng
-	Nâng cấp tìm kiếm lịch sử chat theo tên user, sđt (chính xác), email (chính xác), tag người dùng
-	Chat + Inbox bình luận cho phép gửi ra quá 1000 ký tự từ mẫu trả lời. Hiển thị trên caresoft sẽ tự tách làm 2 tin nhắn
-	Bổ sung yêu cầu đăng nhập phải tick captcha mới cho đăng nhập
-	Bổ sung thanh cuộn cho các option kịch bản nhiều hơn 7 tùy chọn
-	Nâng cấp định tuyến thoại gọi vào gọi ra
-	Update cấu hình upload tin nhắn sticker cho page facebook, lỗi do chính sách Facebook cho EU Change log
-	Bổ sung chọn quyền view ticket filter theo nhóm (trong cấu hình filter ticket). [Ảnh](https://prnt.sc/12m7xny)
-	bổ sung cấu hình cho phép tìm kiếm phiếu ghi trước khi tìm kiếm ở phần tất cả. Chỉnh sửa lại giao diện cho dễ nhìn hơn
-	Bổ sung UX: Gửi tin nhắn ra từ bình luận cho phép nhấn "Enter" để gửi thay bằng nút "Gửi" như hiện tại
-	Bổ sung cho phép gửi file đính kèm lớn hơn hiện tại Caresoft là 5Mb Fb giới hạn 25Mb
-	Bổ sung thêm điều kiện tạo Filter ticket luôn kèm trạng thái mặc định là khác CLOSE. [Ảnh](https://prnt.sc/12m80c6)
-	Thêm tính năng paste ảnh ở giao diện chat client
-	Chat bổ sung lưu báo cáo Inbox chuyên viên ngay khi Agent tiếp nhận (Cấu hình tạo ticket khi tiếp nhận chat)
-	Bổ sung báo lỗi khi khách hàng import file chứa ngày tháng không đúng định dạng (năm/tháng/ngày) vào trường động
-   Bổ sung chỉ số thời gian từ lúc chuyên viên tiếp nhận đến lúc chuyên viên phản hồi lại tin nhắn đầu tiên cho khách hàng trong báo cáo Facebook - Thời gian phản hồi
-   Bổ sung thời lượng tìm kiếm báo cáo chiến dịch - xử lý ticket khác
-   Bổ sung chế độ nghe gọi qua App CareSoft: https://kb.caresoft.vn/support/hc#/?type=article&id=4086
-   Bổ sung trường dữ liệu tình trạng gọi/chat vào API getTickets - ticket_source_end_status. Giá trị tham chiếu Voice, chat miss thì có giá trị là 1, default là 0
-   Bổ sung dữ liệu lý do miss call trong API getCalls( Danh sách lịch sử cuộc gọi) - missed_reason.Giá trị tham chiếu: 
    - missed_customer: Nhỡ bởi khách, 
    - missed_agent_timeout: Nhỡ do không bắt máy, 
    - missed_agent_device: Nhỡ do thiết bị,  
    - missed_agent_reject: Nhỡ do từ chối
-   Bổ sung không ghi đè trường động ghi merge vào ticket cũ đã setup thuộc tính động

### 2. Fixed
-	Fix lỗi tạo chiến dịch bằng tay với ngày tạo
-	Không cập nhật được trường cha trong phần cấu hình thuộc tính người dùng
-	Không lưu báo cáo lịch sử cuộc gọi cho chuyên viên được chuyển khi ấn chuyển nội bộ
-	Lưu báo cáo cuộc gọi tiếp nhận bị lưu sai cột người tiếp nhận cuối cùng
-	Fix danh sách agent đổi từ tìm kiếm trên solr sang DB, tìm End-user vẫn tìm trên solr
-	Fix lỗi tìm ticket trong segment theo điều kiện sau, trước bao nhiêu ngày
-	Fix lỗi agent gửi link ra ngoài tin nhắn không bấm được
-	Fix lỗi timeout Solr (nâng giới hạn lên 60 giây)
-	Fix lỗi search API chi tiết cuộc gọi theo index call_id
-	Fix lỗi import người dùng vào chiến dịch không lưu cột “Agent sẽ nhận phiếu khi tạo campaign (Email)
-	Fix lỗi cột ticket không ghi nhớ ẩn hiện sau khi đổi filter cho các filter cố định cũ.
-	Fix bug tin nhắn offline không lưu phiếu ghi vào hội thoại
-	Live chat fix lỗi agent chat cho visitor khi visitor đang tải trang bị mất tin nhắn
-	Fix lỗi không hiển thị avatar agent khi visitor tải lại trang
-	Fix lỗi visitor gửi chat lên agent bị hiển thị thông tin thất bại
-	Fix lỗi lẫn thông tin phiếu ghi của người yêu cầu này vào 1 người yêu cầu khác khi thao tác quá nhanh
-	Fix lỗi cuộc gọi IVR → ACD → Voicemail lỗi hiển thị số người gọi
-	Fix tìm kiếm theo số phiếu ghi trong segment ticket
-	Fix lỗi list hiển thị tickets nếu cho hiển thị trường động là dạng ngày tháng bị mất
-	Fix bug Zalo không gửi tin nhắn ra được với OA kích hoạt API version 2
-	Fix lỗi không thay đổi được thông tin livechat (khách hàng nhúng sẵn tên vào ô chat nhưng khi mở ra lại hiện userXXXX thay vì tên khách hàng)
-	[Chat widget] Fix lỗi không sửa được thông tin user khi chat lên
-	[Chat widget] Fix lỗi chat khi chưa nhập thông tin trên IOS sẽ bị che mất phần nhập thông tin do bị phím ảo chèn lên
-	Fix bug Chat 1 số case không update người xử lý
-	Fix lỗi chọn trường con dạng chọn nhiều, sau khi f5 thì không hiển thị
-	Fix lỗi nội dung comment bị duplicate khi nhận email vào
-	Fix lỗi trường động phiếu ghi chèn biểu tượng đặc biệt (Emoji)
-	Fix survey qua chiến dịch không ghi nhân trong báo cáo
-	Fix lỗi các báo cáo SLA (test tất cả các báo cáo SLA): phiếu ghi từ nguồn SMS OUT, EMAIL OUT (tính cả gửi từ campaign, auto message, manual message), Thoại ở chế độ không đăng nhập (các trường hợp phiếu ghi tạo ra mà solved luôn) không được gán theo sla nào cả -> không được tính là đạt hay không đạt
-	Fix lỗi Sai trạng thái kết thúc khi chuyển survey
-	Fix lỗi tự động điền vào ticket subject và tự save password khi tạo mới ticket
-	Fix người dùng sắp xếp theo thứ tự theo tên không đúng
-	Fix Import người dùng không cho ghi đè email chính của Agent
-	Fix lỗi ko hiển thị kết quả SLA ở phần Bộ lọc
-	Fix lỗi lưu sai thời gian phản hồi lần đầu của phiếu ghi
-	Fix lỗi smartdialer im lìm khi agent nhấc máy
-	Fix lỗi không cập nhật trạng thái campaign khi tạo hành động sms và email
-	Fix lỗi hiển thị sai ảnh bài viết
-	Fix lỗi Admin không tác động được trạng thái ticket của agent dùng chế độ không cần đăng nhập
-	Fix lỗi tính năng chia việc khi chọn thời gian ngày chủ nhật
-	Lỗi báo cáo theo trường động ở báo cáo chiến dịch
-	Fix lỗi tạo tk sale solution, fix export báo cáo QA
-	Fix lỗi trạng thái gọi nhỡ của phiếu ghi gọi ra
-	Fix bug Tạo phiếu ghi Zendesk lỗi
-	Fix lỗi sai service live chat khi Agent active chat
-	Fix lỗi gán phiếu ghi last agent bị sai khi tất cả agent đều off trạng thái phiếu ghi
-	fix lỗi zendesk: 1 cuộc gọi ra của KH tự động sinh ra 2 ticket trên Zendesk
-	Fix lỗi Báo cáo Phiếu ghi theo nguồn
-	Fix lỗi: KB không lưu được chuyên mục con và tìm kiếm trên KB ra bài viết khác của khách hàng
-   Fix lỗi 1 số email nhận vào bị thiếu nội dung
-	Fix lỗi thoại im lìm khi chuyển khảo sát
-	Fix lỗi giám sát khi agent đang nghe gọi sau đó f5 lại thì mất thông tin nghe gọi của agent đó
-	Fix lỗi báo cáo chi tiết zalo không bấm mở phiếu ghi được
-	fix lỗi bị duplicate phiếu ghi khi agent để trả lời tự động
-	Lỗi hiển thị bài viết, không kéo xem được các bài viết khác khi để màn hình full
-	Fix lỗi loop Automessage
-	Fix lỗi ban ip khách hàng, lưu redis sai dẫn đến kh bị ban vẫn hiển thị box chat
-	Fix lỗi báo cáo lịch sử chat không hiển thị thông tin user đã bị merge
-	Fix lỗi Automessage: test lại các luồng automessage và chia phiếu ghi
-	Fix lỗi vòng lặp vô tận khi lưu segment
-	Fix bug lưu báo cáo Agent active chat khi đang phân phối
-	Fix bug Lưu sai dịch vụ chat nhỡ
-	Sửa url popup khi cập nhật email
-	Fix lỗi không thêm được email mới gmail
-	Fix lỗi gọi ra agent để chế độ divert cuộc gọi
-	Sửa lỗi Bộ lọc không nhận giá trị 0 khi lưu bộ lọc với kiểu text (Họ tên, Tiêu đề, Tên tổ chức…)
-	Fix lỗi lưu đúng tên người chat theo cấu hình người gửi trong cấu hình Trigger
-	Fix lỗi không chuyển chat offline khi có agent không có kỹ năng trên dịch vụ livechat offline. Support trả về số lượng online theo từng service livechat trên domain
-	Fix bug báo cáo chat nhỡ không ghi nhận theo bộ phận
-	Lỗi tìm kiếm theo bộ phận trong báo cáo lịch sử cuộc gọi ,không chọn được bộ phận có tên ngắn hơn 2 ký tự
-	Fix lỗi báo cáo: Báo cáo Tổng cuộc gọi ra (chuyên viên có dữ liệu mới hiển thị), Báo cáo Facebook->Tin nhắn bỏ lỡ (lỗi view chi tiết khi click vào Tin nhắn nhỡ và liên hệ lại)
-	Fix lỗi giám sát thoại - Số cuộc gọi chờ và số cuộc gọi đang tiếp nhận hiển thị không đúng
-	Fix lỗi đóng bình luận, đóng chat , các trường kiểu văn bản để trống nhưng khi mở phiếu ghi lại hiển thị “br br”
-	Fix lỗi báo cáo lịch sử cg không chọn được page size
-	Fix hiển thị tooltip tên group trong phần chọn group ticket
-	Fix lỗi email bị tách phiếu ghi trong trường hợp gửi email ra ngoài
-	Fix lỗi báo cáo khách hàng theo trường động
-	Tìm kiếm kỹ năng chuyên viên không hiển thị kỹ năng IVR
-	Báo cáo QA gửi sai ngày tháng khi chọn khung thời gian “tùy chọn”
-	Fix lỗi load trường động khi tìm kiếm nhanh ticket (đối với trường động có số bản ghi quá lớn)
-	Fix lỗi chọn trường động cha con trong Segment Ticket(đối với trường động có số bản ghi quá lớn)
-	Fix lỗi Smartdialer
    -	Tính năng xóa kịch bản (Đối với kịch bản nháp, Kịch bản chưa chạy lần nào, Kịch bản lỗi). Có thể khôi khục lại kịch bản đã xóa
    -	Fix lỗi nghe file ghi âm của predictive call hoặc auto call nếu có
    -	Fix lỗi báo cáo tổng quan cho phép chọn báo cáo của ngày hiện tại.
    -	Fix giao diện. Gom tất cả cuộc gọi có mã ngoài cuộc gọi thành công và cuộc gọi bận máy thành “Cuộc gọi không liên lạc được”
-	Fix lỗi thoại gọi vào hold rồi transfer bị ngắt cuộc gọi luôn
-	Fix lỗi lưu sai thời gian báo cáo cuộc gọi: các phần gọi vào, gọi ra, gọi ra bấm trực tiếp từ ipphone
-	Fix lỗi tìm kiếm nhanh nâng cao. Chọn trường động cha cho phép không chọn trường động con
-	Fix lỗi kịch bản Email khảo sát không hiển thị file đính kèm khi cập nhật
-	fix lỗi báo cáo email không hiển thị dữ liệu khi chọn loại email
-	fix lỗi giao diện báo cáo tổng quan phần khung giờ bị nhảy chữ ở màn hình bé
-	Fix lỗi thêm mới page zalo bị gửi 2 lần cập nhật
-	fix lỗi trường thuộc tính động kiểu số không hiển thị trong màn hình user cschat
-	fix lỗi chat bot cs
-	Fix lỗi giám sát chat agent sai thời gian trả lời trung bình
-	Fix lỗi phát nhạc chờ không phát được nhiều file
-	Fix lỗi thỉnh thoảng lưu sai thời gian đối với các cuộc gọi ra bấm từ giao diện
-	Fix lỗi cuộc gọi chuyển trao đổi nội bộ không ngắt nhạc khi 1 người dùng softphone/ipphone bấm ngắt trên giao diện CS
-	Fix bug đóng chat từ 1 phiếu ghi open không lưu thời gian đóng phiếu
-	Fix lỗi lọc bình luận qua post id không hiển thị dữ liệu
-	Fix lỗi tạo ticket đồng bộ với chat nhỡ
-	Fix lỗi Chat notify mobile check mất kết nối mới gửi notify điện thoại
-	Fix lỗi lẫn thông tin phiếu ghi của người yêu cầu này vào 1 người yêu cầu khác khi thao tác quá nhanh
-	Fix lỗi bị ngắt cuộc gọi khi reconnect websocket
-	Sửa một số lỗi khi chuyển cuộc gọi và chuyển gọi trao đổi nội bộ khi bấm trực tiếp từ ipphone
-	Ticket chat fix lỗi không cập nhật các trường động khi bị ghép vào phiếu ghi gần nhất (mặc định 24h)
-	Fix lỗi gửi campaign, manualmessage và automessage sms
-	Fix lỗi bấm gọi ra trực tiếp từ ipphone thỉnh thoảng bị sai múi giờ (test thì kiểm tra báo cáo, tín hiệu 2 chiều)
-	Fix lỗi import/export người dùng giá trị ở cột thành phố, quận huyện
-	Lỗi không bật trigger trên trình duyệt safari
-	Fix lỗi khi update facebook không hiển thị các trường dữ thông tin đồng bộ nếu lúc thêm chọn 60 , 90, 180 ngày ( Cấu hình facebook)
-	Fix lỗi hiển thị sắp xếp đơn hàng và Invoice của Kios Việt (Đang bị sắp xếp từ cũ đến mới, cần đảo ngược lại)
-	fix bug đánh dấu chưa phản hồi nếu tin nhắn cuối cùng của khách hàng
-	fix lỗi nhúng crm trong 1 vài trường hợp sẽ không mở popup khi có cuộc gọi đến
-	Lỗi không hiển thị ảnh trong email gửi ra
-	Lỗi bị nhầm thông tin phiếu ghi khách hàng trên giao diện lịch sử tương tác
-	Lỗi không hiển thị boxchat trên trình duyệt ẩn danh
-	Kiểm tra dạng số khi gửi dữ liệu về CPS với comment_id
-	Fix lỗi lọc bình luận chưa phản hồi không ra dữ liệu
-	Fix lại lỗi live chat mất thông tin khi khách hàng chưa thực hiện nhúng lại chat (khách hàng chưa nhúng lại chat sẽ vẫn dùng bình thường nhưng khi chuyển sang tab ẩn danh sẽ bị mất thông tin)
-	Fix lỗi index automation
-	Fix lỗi lưu thiếu nội dung cuộc gọi (comment) khi đang gọi ra (bấm trực tiếp ipphone) mà agent đăng xuất (test dữ liệu lịch sử cuộc gọi, comment các TH gọi ra gọi vào)
-	fix lỗi không hiển thị bình luận từ 1 ticket thuộc group đã bị xóa
-	Fix lỗi tạo kịch bản IVR
-	Fix bug lỗi chat facebook khi gửi tin nhắn chứa cả ảnh và text thì mất text
-	Fix bug 1 số trường hợp tiếp nhận chat không view thông tin chat cũ phải f5 (case chat cũ khởi tạo từ việc khách hàng click vào 1 button)
-	Fix bug 1 số case duplicate tin nhắn Inbox bình luận khi event mất đồng bộ thứ tự
-	Fix lỗi tạo tin nhắn mới nếu tắt bật tính năng reply sms nhảy qua các tab
-	Fix lỗi thêm tag/ Xóa tag và set tag trong Trigger/ Automation (Giao diện cấu hình)
-	Fix Chuyên viên không được gán kỹ năng khi thêm vào bộ phận có kỹ năng sẵn
-	Fix lỗi đăng xuất đổi domain trên caresoft bị đổi thông tin user trong phần chat in-app
-	Fix lỗi báo cáo top khách hàng không ra dữ liệu
-	Fix lỗi bị sai thông tin trường trạng thái kết thúc ở báo cáo và dữ liệu xuất ra
-	Fix lỗi không Update xóa ngày tháng trong trường động người dùng
-	fix lỗi báo cáo chiến dịch chi tiết chọn tin nhắn tự động không hiển thị các trường thông tin câu hỏi trong kịch bản
-	Fix bug ghép người dùng Facebook mất lịch sử chat
-	Chat ticket bỏ lưu trường is_overdue = 0 fix lỗi báo cáo sla
-	tích hợp app zendesk fix lỗi không gửi được link file ghi âm khi gọi ra, gọi vào , bổ sung tool cập nhật link file ghi âm thủ công
-	Fix lỗi gọi ra bấm trực tiếp từ ipphone, gọi nội bộ bấm trực tiếp từ ipphone, gọi nội bộ từ web thỉnh thoảng bị dừng đột ngột
-	Fix lỗi tìm kiếm lịch sử zalo theo zalo page không ra kết quả
-	Fix lỗi: Validate thời gian chạy quá dài của chiến dịch tự động (Quá 10 năm)
-	Facebook deactive page khi gặp lỗi tk bị checkpoint
-	Fix bug bỏ qua đánh dấu chưa phản hồi khi đồng bộ tin nhắn cũ
-	Fix lỗi Quy tắc tự động hóa: Tạm ngưng 1 quy tắc, bổ sung Thời điểm tạo ticket trong điều kiện, Tiêu đề email thiếu trong Trigger
-	Fix lỗi update kich bản IVR
-	fix lỗi báo cáo agent theo trạng thái không phân trang được
-	Fix lỗi gửi ảnh landscape khi zoom vào không phóng to được (cschat)
-	Fix lỗi tạo kịch bản IVR.
-	Fix lỗi tin nhắn có người đang xử lí trong lịch sử chat có màu đen (cũ có màu xanh)
-	Fix lỗi báo cáo lịch sử zalo không hiển thị chuyên viên tiếp nhận
-	Fix lỗi không tạo được comment của admin khi domain không có user admin + admin trả lời bình luận bằng giao diện fb
-	fix lỗi box confirm đóng chat ,đóng bình luận không ấn enter để đóng được
-	fix lỗi báo cáo lịch sử cuộc gọi không sort được
-	Fix lỗi performance phần giám sát, bổ sung filter theo common status, bổ sung các icon thể hiện các trạng thái chung ở phần monitor thoại
-	fix lỗi validate file đính kèm trong phần ticket form
-	fix lỗi lặp tin nhắn zalo nếu cùng 1 user chat lên 2 OA
-	Live chat fix giao diện button chat bot bị quá to khi nhập nhiều thông tin
-	Fix lỗi tìm kiếm trong segment Ticket, tìm theo chiến dịch/m “Có giá trị” và “Không có giá trị”
-	Fix lỗi Google Analytics lặp event Agent join chat
-	fix lỗi không hiển thị thời gian khách hàng nhắn tin
-	Chat fix bug tạo 2 User khi người dùng chat vào từ quảng cáo Facebook
-	Facebook fix lỗi không nhận tin nhắn bấm nút khi Page dùng nhiều App Facebook và App Caresoft không được đặt ưu tiên
-	fix lỗi không hiển thị dữ liệu AHT Voice trên bảng điều khiển
-	fix lỗi hiển thị voice khi tài khoản không có chức năng call
-	Fix lỗi cuộc gọi chuyển từ agent A → B thì cuộc gọi của B chưa được lưu nhánh dịch vụ vào báo cáo cuộc gọi
-	fix lỗi giao diện, chuyển chế độ nghe gọi sang di động, sau đó bị mất kích hoạt thoại vẫn hiển thị bộ đếm thời gian cuộc gọi
-	Live chat fix lỗi chat bot menu bị nhảy sang ngang khi nội dung trong button quá ngắn
-	Fix lỗi duplicate menu chat bot trong phần Tùy chọn
-	Fix lỗi không hiển thị menu chatbot trong phần Tùy chọn khi khách hàng trả lời trigger
-	Lỗi hội thoại khách hàng bị lặp trong phần lịch sử chat
-	Không tìm kiếm được khách hàng trong lịch sử chat
-	Lỗi báo cáo tổng quan (Khách hàng tạo theo nguồn)
-	Lỗi hiển thị báo cáo tổng quan (Phiếu ghi được tạo theo trường thuộc tính)
-	Chat fix bug không update tin nhắn cuối cùng khi gửi file.
-	Bổ sung validate ký tự tối đa trong trường động ticket (max 255) khi cập nhật ticket, đóng chat
-	Thêm thông báo khi tin nhắn bị chặn không gửi được
-	Bổ sung trả về messages cho client live chat khi f5
-	Bỏ cơ chế tự động mời chat cũ
-	Loại bỏ phần "chọn dịch vụ hỗ trợ" trên widget của boxchat
-	Bổ sung phần check trạng thái offline chat chuyên viên để ghi nhận đúng thông báo nhỡ và offline chat
-	Điều chỉnh app Zoho về ticket (thay vì ở user như yêu cầu ban đầu)
-	Ivr node nhập liệu: sửa lại subject ticket: bổ sung tên node IVR
-	Thay đổi app cmsedu theo yêu cầu của khách hàng
-	Loại bỏ chức năng export biểu đồ báo cáo do lỗi render hình ảnh của lib bị bug khi label quá dài
-	thay đổi định dang file ghi âm (ogg -> mpeg)
-	Fix hiển thị cho apps CRM EDU
-	Sửa báo cáo Khách hàng tương tác theo kênh
-	Bổ sung Smartdialer Predict call gọi khách hàng trước fix điều kiện cuộc gọi thành công chỉ khi agent đã bắt máy
-	Facebook bổ sung cơ chế tự động bỏ cấu hình thẻ tag khi phát hiện gửi ra bằng thẻ tag lỗi. Tránh KH dùng sai mục đích lock Page
-	ẩn báo cáo chiến dịch theo kịch bản do không cần thiết, đã có báo cáo chiến dịch chi tiết thay thế
-	sửa lại giao diện chat bot facebook cho các content có slide, website, button, product. Bổ sung cho các tin nhắn từ khách
-	Sửa tên dịch vụ Voice -> Voice in
-	Sửa delay time trigger từ 30s thành 10s
-	sửa label báo cáo tổng quan - khách hàng tạo theo nguồn: Voice -> Voice in
-	Turning báo cáo thoại nhỡ
-	Thay đổi cơ chế load interval chat để giảm tải hệ thống
-	Tối ưu lại code gather Zalo fix leak RAM + miss tin đồng bộ
-	Fix lỗi JS không reload lại filter khi tắt tab ticket
-	Fix lỗi JS hiển thị lỗi khi timeout filter
-	Fix lỗi XSS khi ticket được tạo từ API bị dính script bẩn. (API COMMENT JAMJA)
-	Fix bug tên agent khi chuyển chat quá max length DB (bỏ cộng agent -> lưu agent cuối cùng)
-	Sửa lỗi XSS khi hiển thị ticket comment (chèn mã js vào Onload Image
-	Tuning câu lệnh sql trong phần gán kỹ năng chuyên viên
-	Sửa lại label các báo cáo: Thoại, Live Chat, Inbox Facebook, Zalo , trong các báo cáo tổng cuộc gọi / chat / facebook / zalo vào, miss
-	Fix lỗi đánh index solr chia phiếu ghi trong trường hợp phiếu tạo từ API
-	Fix lỗi api get contacts
-	Sửa lại cơ chế đồng bộ facebook
-	Fix lại button mở apps tích hợp sẽ mở được phần apps kể cả khi không có kịch bản chiến dịch
-	Fix numPage Api
-	Fix bug không gửi survey facebook với chat người dùng mới
-	Fix lỗi API danh sách chat conversation ID của khách chat không map với contact ID (Kiểm tra và dựng case trong API docs, lấy danh sách chat, lấy 1 chat bất kỳ, lấy customer_id trong đó ra và getContacts → lỗi)
-   Bỏ route báo cáo v1, tất cả các api gọi vào báo cáo v1 sẽ trả về mã 400 deprecated
-	Tối ưu câu lệnh phần lấy dữ liệu bình luận
-	Fix hiển thị comment trong ticket cho phép các comment thiếu ID người tạo có thể hiển thị (Một số comment được tạo tự động từ SMS hoặc API)
-	Tối ưu tốc độ import Người dùng vào bộ lọc
-	Tối ưu update user chat DB
-	Tối ưu tốc độ đồng bộ realtime chat facebook
-   Fix lỗi App Mobile không hiển thị được mẫu trả lời phiếu ghi do thay đổi cơ chế hiển thị trên Web
-   Fix lỗi không hiển thị dịch vụ Ticketform trong cấu hình kỹ năng
